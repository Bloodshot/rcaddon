#include "script_component.hpp"

class CfgPatches {
    class ADDON {
        name = COMPONENT_NAME;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = {
            "A3_Data_F_Mod_Loadorder",
        };
        author = "Bloodshot";
        authors[] = {};
        url = "rainbowcoalition.net";
        VERSION_CONFIG;
    };
};

#include "CfgMods.hpp"
#include "CfgEventHandlers.hpp"

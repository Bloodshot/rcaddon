#include "..\script_component.hpp"

params [["_funcFile", "", [""]], ["_funcName", "", [""]]];

uiNamespace setVariable [_funcName, compileScript [_funcFile, false]];
missionNamespace setVariable [_funcName, uiNamespace getVariable _funcName];

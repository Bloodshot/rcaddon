#include "\x\cba\addons\main\script_macros_common.hpp"
#include "\x\cba\addons\xeh\script_xeh.hpp"

#define OPREP(fncName) [QPATHTOF(functions\DOUBLES(fnc,fncName).sqf), QFUNC(fncName)] call rc_fnc_compileOverridable
#define SUPER(fncName) (uiNamespace getVariable QFUNC(fncName))
#define ESUPER(addon,fncName) (uiNamespace getVariable QEFUNC(addon,fncName))

#ifdef DISABLE_COMPILE_CACHE
    #undef PREP
    #define PREP(fncName) FUNC(fncName) = compile preprocessFileLineNumbers QPATHTOF(functions\DOUBLES(fnc,fncName).sqf)

    #undef PREPMAIN
    #define PREPMAIN(fncName) TRIPLES(PREFIX,fnc,fncName) = compile preProcessFileLineNumbers QPATHTOF(functions\DOUBLES(fnc,fncName).sqf)
#else
    #undef PREP
    #define PREP(fncName) [QPATHTOF(functions\DOUBLES(fnc,fncName).sqf), QFUNC(fncName)] call CBA_fnc_compileFunction

    #undef PREPMAIN
    #define PREPMAIN(fncName) [QPATHTOF(functions\DOUBLES(fnc,fncName).sqf), 'TRIPLES(PREFIX,fnc,fncName)'] call CBA_fnc_compileFunction
#endif

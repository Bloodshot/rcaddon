class CfgMods {
    class PREFIX {
        author = "Bloodshot";
        dir = "@rcaddon";
        name = "Rainbow Coalition v0.0.0";
        hidePicture = 1;
        hideName = 1;
        tooltip = "Rainbow Coalition";
        tooltipOwned = "Rainbow Coalition Owned";
    };
};

class CfgNotifications
{
  class Update
  {
    title = "Update";
    iconPicture = QPATHTOF(assets\info.paa);
    iconText = "";
    description = "%1";
    color[] = {1,1,1,1};
    priority = 1;
    duration = 5;
  };
};

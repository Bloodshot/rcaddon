// Generated by genprep, do not edit

// Client
OPREP(addArsenalAction);
OPREP(addArsenalItems);
OPREP(addCrateActionsLocal);
OPREP(addDeployAction);
OPREP(addDiary);
OPREP(addJIPDeployAction);
OPREP(addResetAction);
OPREP(addRespawnEH);
OPREP(addWaypointTracker);
OPREP(adjustCollective);
OPREP(bunkermode);
OPREP(callAdminHelper);
OPREP(canAddCrate);
OPREP(canDeploy);
OPREP(canOpenArsenal);
OPREP(canRepairAircraft);
OPREP(centerMapOnAO);
OPREP(defineAdminHelper);
OPREP(displayCountdown);
OPREP(displayTitle);
OPREP(doDeploy);
OPREP(endCountdown);
OPREP(initFortifyLocal);
OPREP(initGroupEHs);
OPREP(initKeybinds);
OPREP(initPlayerSettings);
OPREP(initRespawnSpectator);
OPREP(initWaypointTracker);
OPREP(mapClick);
OPREP(mapClick_map);
OPREP(mapClick_onClick);
OPREP(mapClick_zeus);
OPREP(openArsenal);
OPREP(recenterMap);
OPREP(removeHomeActions);
OPREP(setACREChannels);
OPREP(setRadarIcon);
OPREP(shouldBFTTrack);
OPREP(shouldTrackWaypoint);
OPREP(snowPFH);
OPREP(snowRoofHandler);
OPREP(startSnow);
OPREP(updateBFTMarkers);

// Other
OPREP(mapClick_intersection);

// Client / Server
OPREP(addCrates);
OPREP(clearCargo);
OPREP(dirToCardinal);
OPREP(eachMarker);
OPREP(emptySeats);
OPREP(generateIntermediatePosition);
OPREP(getDeployMarker);
OPREP(getRoleIcon);
OPREP(humanDistance);
OPREP(initDynamicSimulationLocal);
OPREP(initSupplyBox);
OPREP(initSupplyCrate);
OPREP(initSupplyCrateLocal);
OPREP(isHome);
OPREP(limitMedical);
OPREP(loadVehicleCrates);
OPREP(nearestPlayer);
OPREP(spawnSupplyBox);
OPREP(spawnSupplyCrate);
OPREP(underRoof);

// Server
OPREP(addCrateActions);
OPREP(addDeployActionToTopLeader);
OPREP(addDispersionEH);
OPREP(addZeusObjects);
OPREP(aiDeathInventory);
OPREP(aiFlashlightsOff);
OPREP(createInsertHelo);
OPREP(createPlane);
OPREP(createZeus);
OPREP(deploy);
OPREP(deployJIP);
OPREP(disembarkVehicle);
OPREP(doParadrop);
OPREP(emptyAllVehicleCargo);
OPREP(fillTransportSlots);
OPREP(garrisonBuildings);
OPREP(garrisonNearby);
OPREP(haltEditorUnits);
OPREP(helicopterEmbark);
OPREP(holdPosition);
OPREP(initAiDeathInventory);
OPREP(initDynamicSimulation);
OPREP(initFortify);
OPREP(initHelicopterActions);
OPREP(initTemplates);
OPREP(loadCrates);
OPREP(muteAllUnits);
OPREP(paradropTracker);
OPREP(populateEnemies);
OPREP(spawnCrew);
OPREP(spawnEnemyInfantry);
OPREP(spawnExtract);
OPREP(spawnGroup);
OPREP(spawnGuards);
OPREP(spawnParatroopers);
OPREP(spawnPatrol);
OPREP(spawnRandomArmorGroup);
OPREP(spawnSupplyDrop);
OPREP(spawnVehicle);
OPREP(spawnVehicleFromPool);
OPREP(spawnWave);
OPREP(startMission);
OPREP(watchAV);
OPREP(watchTroopTruck);

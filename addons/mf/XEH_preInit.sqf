#include "script_component.hpp"

#include "XEH_PREP.inc"

GVAR(allClients) = [-2, 0] select hasInterface;

if (fileExists "buildingPositions.sqf") then {
  GVAR(buildingPositions) = createHashMapFromArray ([] call compile preprocessFileLineNumbers "buildingPositions.sqf");
};

GVAR(adminHelpers) = createHashMap;

ADDON = true;

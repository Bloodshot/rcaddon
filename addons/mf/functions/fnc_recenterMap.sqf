#include "..\script_component.hpp"
/*
 * Center map controls around a given point
 *
 * Arguments:
 * 0: Position 2D of desired map center <ARRAY>
 * 1: Desired zoom level <NUMBER>
 *
 * Return:
 * Whether a map control was found <BOOLEAN>
 *
 * Example:
 * [markerPos "aoMarker", 0.8] call rc_mf_fnc_recenterMap
 *
 * Caller: Client
 */

params ["_center", "_zoom"];

private _idd = switch (true) do {
  case (!isNull findDisplay 37):  {37};  // RscDisplayServerGetReady
  case (!isNull findDisplay 53):  {53};  // RscDisplayClientGetReady
  case (!isNull findDisplay 52):  {52};  // RscDisplayServerGetReady
  case (!isNull findDisplay 312): {312}; // RscDisplayCurator
  case (!isNull findDisplay 12):  {12}; // RscDisplayMainMap
  default {nil};
};

if (isNil "_idd") exitWith {false};

private _display = findDisplay _idd;
private _mapControl = _display displayCtrl 51;
_mapControl ctrlMapAnimAdd [0, _zoom, _center];
ctrlMapAnimCommit _mapControl;

true

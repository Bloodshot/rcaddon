#include "..\script_component.hpp"
/*
 * Convert an angle in degrees to a cardinal direction string, using 8 or 16
 * cardinal directions such as "northeast".
 *
 * Arguments:
 * 0: Angle, in degrees, clockwise from north <NUMBER>
 * 1: Whether to use intercardinal directions such as north-northeast (default: false) <BOOLEAN>
 *
 * Return:
 * String containing a cardinal direction approximating the given angle <STRING>
 *
 * Example:
 * [30] call rc_mf_fnc_dirToCardinal // "northeast"
 *
 * Caller: Client / Server
 */

params ["_dir", ["_intercardinals", false]];

if (_intercardinals) then {
  ["north", "north-northeast", "northeast", "east-northeast", "east",
   "east-southeast", "southeast", "south-southeast", "south", "south-southwest",
   "southwest", "west-southwest", "west", "west-northwest", "northwest",
   "north-northwest"] select (floor (((_dir mod 360) + 11.25) / 22.5));
} else {
  ["north", "northeast", "east", "southeast", "south",
   "southwest", "west", "northwest"] select (floor (((_dir mod 360) + 22.5) / 45));
};

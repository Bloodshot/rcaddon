#include "..\script_component.hpp"
/*
 * Spawn a wave of infantry and vehicles, randomly in a circular sector around
 * the spawn position, and order them to attack the target position via a set of
 * intermediate rally points.
 *
 * See rc_mf_fnc_spawnRandomArmorGroup for a description of the armorFactor
 * argument.
 *
 * Arguments:
 * 0: Spawn position in Position ATL <ARRAY>
 * 1: Target position in Position ATL <ARRAY>
 * 2: Groups' side <SIDE>
 * 3: Number of groups to spawn <NUMBER>
 * 4: Armor factor to use when spawning groups <NUMBER>
 * 5: Angular spread of spawned groups from their destination, in degrees <NUMBER>
 *
 * Return:
 * List of spawned groups <ARRAY>
 *
 * Example:
 * [markerPos "enemyWaveSpawn", markerPos "friendlyBase", east, 5, 1, 80] call rc_mf_fnc_spawnWave
 *
 * Caller: Server
 */

params [
  "_position", "_targetPos", "_side",
  ["_count", 3], ["_armorFactor", missionNamespace getVariable ["armorFactor", 1]],
  ["_spread", 45]
];

private _dir = _position getDir _targetPos;
private _distance = _position distance2D _targetPos;

private _wpAngle = (random 135) + (random 135) - 135;
private _wpDist = (random 300) + 200;
private _wpPos = [_position, _targetPos, _wpDist, _wpAngle] call FUNC(generateIntermediatePosition);
_wpPos set [2, 0];

private _groups = [];

private _failures = 0;

for "_i" from 1 to _count do {
  private _groupDir = _dir + (random _spread) - (_spread / 2);
  private _groupDist = _distance + (random 500) - 250;
  private _spawnPos = _targetPos vectorAdd ([[0, -_groupDist], -_groupDir] call BIS_fnc_rotateVector2D);
  _spawnPos set [2, 0];
  private _group = [_spawnPos, _wpPos, _side, _armorFactor] call FUNC(spawnRandomArmorGroup);

  if (isNil "_group") then {
    _failures = _failures + 1;
    if (_failures < 10) then { _i = _i - 1};
  } else {
    private _wp = _group addWaypoint [_targetPos, 100];
    _wp setWaypointType "SAD";

    _groups pushBack _group;
  };
};

_groups

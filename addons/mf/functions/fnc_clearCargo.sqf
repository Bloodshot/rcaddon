#include "..\script_component.hpp"
/*
 * Clear all vehicle cargo
 *
 * Arguments:
 * 0: Vehicle <OBJECT>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [box] call rc_mf_fnc_clearCargo
 *
 * Caller: Client / Server
 */

params ["_veh"];

clearWeaponCargoGlobal _veh;
clearMagazineCargoGlobal _veh;
clearBackpackCargoGlobal _veh;
clearItemCargoGlobal _veh;

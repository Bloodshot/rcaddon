#include "..\script_component.hpp"
/*
 * Call a function previously defined by rc_mf_fnc_defineAdminHelper.
 * Wraps the code, setting the _adminHelper local variable to the name
 * of the helper, and also triggers an event.
 *
 * This is mostly for internal use. An alias should be created for each defined
 * helper that you may call directly.
 *
 * Arguments:
 * 0: Name of the helper <STRING>
 * 1: Arguments to the helper <ARRAY>
 *
 * Return:
 * Return value of the helper
 *
 * Example:
 * ["startPhase2"] call rc_mf_fnc_callAdminHelper
 *
 * Caller: Client
 */
params ["_name", ["_args", []]];

private _helper = GVAR(adminHelpers) get _name;
if (isNil "_helper") exitWith {};

private _ret = _args call _helper;
[QGVAR(adminHelperCalled), [_name, _args]] call CBA_fnc_localEvent;

_ret

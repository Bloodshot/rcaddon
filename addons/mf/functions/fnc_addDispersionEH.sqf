#include "..\script_component.hpp"
/*
 * Add a Fired Event Handler to a unit that will add random dispersion to
 * any fired projectiles. If the unit is mounted, this will apply to all
 * projectiles. If the unit is infantry, it will only apply to explosive
 * projectiles, such as launched grenades and rockets.
 *
 * Missiles with active guidance will seek their original target regardless of
 * dispersion.
 *
 * Arguments:
 * 0: Unit to add dispersion to <UNIT>
 * 1: Dispersion amount, in degrees (default: aiVehicleDispersion, or 0.3) <NUMBER>
 *
 * Return:
 * The ID of the event handler <NUMBER>
 *
 * Example:
 * [gunner _tank, 0.5] call rc_mf_fnc_addDispersionEH
 *
 * Caller: Server
 */

params ["_unit", ["_dispersion", missionNamespace getVariable ["aiVehicleDispersion", 0.3]]];

[_unit, "Fired", {
  params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_magazine", "_projectile", "_vehicle", "_gunner", "_turret"];

  if (isPlayer _unit) exitWith {};
  if ((_vehicle == _unit) && {
    getNumber (configOf _projectile >> "explosive") == 0
  }) exitWith {};

  private _dispersion = _thisArgs select 0;

  [
    _projectile,
    _dispersion * (random 2 - 1),
    _dispersion * (random 2 - 1) + 0.5,
    0
  ] call ace_common_fnc_changeProjectileDirection;
}, [_dispersion]] call CBA_fnc_addBISEventHandler;

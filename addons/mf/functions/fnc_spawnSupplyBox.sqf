#include "..\script_component.hpp"
/*
 * Spawn and initialize a supply box (see rc_mf_fnc_initSupplyBox).
 *
 * Arguments:
 * 0: Spawn position in Position ATL <ARRAY>
 * 1: Supply box class (default: "B_SupplyCrate_F") <STRING>
 *
 * Return:
 * Supply box <OBJECT>
 *
 * Example:
 * [markerPos "supplyMarker"] call rc_mf_fnc_spawnSupplyBox
 *
 * Caller: Client / Server
 */

params ["_pos", ["_class", "B_SupplyCrate_F"]];

private _box = createVehicle [_class, _pos, [], 0, "NONE"];

[_box] call FUNC(initSupplyBox);

#include "..\script_component.hpp"
/*
 * Signal the start of the mission. Resets the time & date to what it was when
 * the mission was loaded, displays the mission title, and does some cleanup.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_startMission
 *
 * Caller: Server
 */

if (missionNamespace getVariable [QGVAR(missionStarted), false]) exitWith {};

missionNamespace setVariable [QGVAR(missionStarted), true, true];
[] remoteExec [QFUNC(displayTitle), GVAR(allClients)];
[] remoteExec [QFUNC(removeHomeActions), GVAR(allClients)];
setDate GVAR(startDate);

private _situationMarkers = getMissionLayerEntities "Situation Markers";

if (count _situationMarkers > 1) then {
  {
    _x setMarkerAlpha 0;
  } forEach (_situationMarkers select 1);
};

{
  _x setCaptive false;
} forEach allPlayers;

// Stop players from stashing extra ammo for themselves later in the mission
// Mostly relevant for missions with prepared positions
{
  deleteVehicle _x;
} forEach (allMissionObjects "GroundWeaponHolder");

[QGVAR(missionStarted)] call CBA_fnc_globalEventJIP;

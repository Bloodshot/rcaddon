#include "..\script_component.hpp"
/*
 * Special logic for AI troop trucks that causes them to disembark under heavy
 * fire, as in an ambush.
 *
 * Will try to drive through light contact or suppressive fire, and unload
 * during ambushes or significant casualties
 *
 * Arguments:
 * 0: Troop truck to watch <VEHICLE>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [troopTruck] call rc_mf_fnc_watchTroopTruck
 *
 * Caller: Server
 */

// Determine when units should unload from a troop truck
params ["_vic"];

private _ehs = [];

if (getNumber (configOf _vic >> "crewVulnerable") == 1) then {
  _ehs pushBack [
    "FiredNear",
    _vic addEventHandler ["FiredNear", {
      params ["_unit", "_firer", "_distance"];

      if (side _firer != side _unit) then {
        _unit setVariable ["bailTendency", 0.01 + (_unit getVariable ["bailTendency", 0])];
      };
    }]
  ];
};

_ehs pushBack [
  "HandleDamage",
  _vic addEventHandler ["HandleDamage", {
    params ["_unit", "_selection", "_damage"];
    if (_damage > 0) then {
      private _delta = linearConversion [0, 0.5, _damage, 0, 0.03, true];
      _unit setVariable ["bailTendency", _delta + (_unit getVariable ["bailTendency", 0])];
    };

    nil
  }]
];

private _time = time;
private _emptySeats = _vic emptyPositions "";
while { !isNull _vic } do {
  private _bailTendency = 0;
  if (isNull (driver _vic) || { !alive (driver _vic) } || { !canMove _vic }) then {
    _bailTendency = 5;
  };

  private _lostPassengers = (_vic emptyPositions "") - _emptySeats;
  _emptySeats = _emptySeats + _lostPassengers;
  _bailTendency = _bailTendency + 0.4 * _lostPassengers;

  _bailTendency = _bailTendency + (_vic getVariable ["bailTendency", 0]);
  private _newTime = time;
  _vic setVariable ["bailTendency", (_bailTendency - (_newTime - _time) * 0.01) max 0];
  _time = _newTime;

  if (isNull _vic) exitWith {};

  // Bail!
  if (_bailTendency > 1 - (damage _vic)) then {
    _vic setUnloadInCombat [true, true];
    { _vic removeEventHandler _x } forEach _ehs;

    [_vic] call FUNC(disembarkVehicle);

    break;
  };
  sleep 5;
};

#include "..\script_component.hpp"
/*
 * Create a plane of the given class to perform a flyover of the destination
 * (for paradrops etc.). If a pilot is given, they will be moved into the driver
 * seat of the plane. Otherwise, a civilian pilot will be created.
 *
 * This function does not handle despawning the plane. That should be done by
 * the caller.
 *
 * Arguments:
 * 0: Destination in Position 2D <ARRAY>
 * 1: Plane class (default: friendlyPlaneClass) <STRING>
 * 2: Spawn position in Position 2D (default: markerPos "planeStart") <ARRAY>
 * 3: Pilot unit (default: nil) <UNIT>
 *
 * Return:
 * [_plane, _pilot] <ARRAY>
 *
 * Example:
 * [markerPos "paradropMarker", friendlyPlaneClass, markerPos "airfieldMarker", myPilot] call rc_mf_fnc_createPlane
 *
 * Caller: Server
 */

params ["_dest", ["_planeClass", friendlyPlaneClass], ["_planeStart", markerPos "planeStart"], ["_pilot", nil]];

if (isNil "_planeClass") exitWith {
  ["No friendlyPlaneClass defined"] call BIS_fnc_error;
};

private _height = [500, 200] select (_planeClass isKindOf "Helicopter");
_height = _height max (_planeStart select 2);
_planeStart set [2, _height];

private _plane = createVehicle [_planeClass, _planeStart, [], 0, "FLY"];
_plane setPosATL _planeStart;
_plane flyInHeight _height;
_plane flyInHeightASL [_height, _height, _height];

_dest set [2, _height];

private _dir = (_plane getDir _dest);
_plane setDir _dir;
_plane setVelocity [66 * (sin _dir), 66 * (cos _dir), 0];
_plane limitSpeed 220;

if (isNil "_pilot") then {
  _pilot = (createGroup civilian) createUnit ["C_Man_Pilot_F", [0, 0, 0], [], 0, "NONE"];
};

_pilot moveInDriver _plane;
(group _pilot) setCombatBehaviour "CARELESS";

(group _pilot) addWaypoint [_dest, -1];
private _secondWP = (vectorNormalized (_dest vectorDiff _planeStart)) vectorMultiply 5000;
_dest = (_dest vectorAdd _secondWP);
_dest set [2, _height];
(group _pilot) addWaypoint [_dest, -1];

[_plane, _pilot];

#include "..\script_component.hpp"
/*
 * Trigger a static-line paradrop from an aircraft.
 * All ViV vehicles will be paradropped, as well as any vehicles contained in
 * the aircraft's rc_mf_doParadrop_dropVehicles variable.
 *
 * Arguments:
 * 0: Aircraft to trigger the paradrop from <VEHICLE>
 * 1: Units to paradrop (default: all units in cargo seats) <ARRAY>
 * 2: Parachute class to use for units (default: "RHS_D6_Parachute") <STRING>
 * 3: Parachute class to use for vehicles (default: "B_Parachute_02_F") <STRING>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [insertPlane, (units squad1) + (units squad2), "Steerable_Parachute_F"] call rc_mf_fnc_doParadrop
 *
 * Caller: Server
 */

params ["_veh", ["_units", nil],
  ["_parachuteClass", "RHS_D6_Parachute"],
  ["_vehicleParachuteClass", "B_Parachute_02_F"]
];

if (isNil "_units") then {
  _units = (
    fullCrew [_veh, "cargo", false] +
    fullCrew [_veh, "turret", false]
  ) apply { _x select 0 };
};

{
  if (vehicle _x != _veh) then { continue };
  _x disableCollisionWith _veh;
  moveOut _x;
  _x setPosASL (getPosASL _x);

  unassignVehicle _x;
  [_x] allowGetIn false;

  _x setDir (getDir _veh);

  sleep 1;

  private _para = _parachuteClass createVehicle [0, 0, 0];
  _para setPosASL (getPosASLVisual _x);
  _para setVectorDirAndUp [vectorDirVisual _x, vectorUpVisual _x];

  if (local _x) then {
    _x moveInDriver _para;
  } else {
    [_x, _para] remoteExecCall ["moveInDriver", _x]
  };

  _x assignAsDriver _para;
  [_x] allowGetIn true;
  [_x] orderGetIn true;
} foreach _units;

{
  _x enableCollisionWith _veh;
} foreach _units;

if (count _units > 0) then {
  sleep 6;
};

{
  private _para = createVehicle [_vehicleParachuteClass, [100, 100, 200], [], 0, "FLY"];
  _para setPosASL ((getPosASL _plane) vectorAdd [0, 0, -20]);

  _x attachTo [_para, [0, 0, 0]];

  sleep 6;
} forEach (_veh getVariable [QGVAR(doParadrop_dropVehicles), []]);

_veh setVariable [QGVAR(doParadrop_dropVehicles), []];

{
  objNull setVehicleCargo _x;
  sleep 6;
} forEach (getVehicleCargo _veh);

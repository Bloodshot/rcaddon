#include "..\script_component.hpp"
/*
 * Returns the surface Zeus has just clicked on
*/

/*
When 2.18 releases:

    private _cameraPos = AGLToASL positionCameraToWorld [0, 0, 0];
    private _ray = screenToWorldDirection getMousePosition;
    private _endPos = _cameraPos vectorAdd (_ray vectorMultiply (viewDistance * 2));

    private _intersections = (lineIntersectsSurfaces [_cameraPos, _endPos]);
    ASLtoAGL (_intersections select 0 select 0);

*/

private _mousePos = screenToWorld getMousePosition;
private _intersections = (lineIntersectsSurfaces [getPosASL curatorCamera, AGLtoASL _mousePos]);
if (count _intersections == 0) then {
  _mousePos;
} else {
  ASLtoAGL (_intersections select 0 select 0);
};

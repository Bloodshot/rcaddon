#include "..\script_component.hpp"
/*
 * Determines whether a unit can use a "load crate" action on a target. Used in
 * the addAction condition added by rc_mf_fnc_addCrateActions.
 *
 * Arguments:
 * 0: Unit using the action <UNIT>
 * 1: Target of the action <VEHICLE>
 *
 * Return:
 * Whether the action should be shown <BOOLEAN>
 *
 * Example:
 * [player, transport] call rc_mf_fnc_canAddCrate
 *
 * Caller: Client
 */
params ["_unit", "_target"];

[_target] call FUNC(isHome);

#include "..\script_component.hpp"
/*
 * Spawn an infantry group and assign them a move waypoint.
 *
 * Arguments:
 * 0: Spawn position in Position ATL <ARRAY>
 * 1: Waypoint position in Position ATL <ARRAY>
 * 2: Side of the group to spawn <SIDE>
 * 3: Number of units to spawn <NUMBER>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [markerPos "spawnPos", markerPos "spawnPos" vectorAdd [100, 100, 0], east, 12] call rc_mf_fnc_spawnEnemyInfantry
 *
 * Caller: Server
 */

params ["_spawnPos", "_targetPos", "_side", "_count"];

private _group = [_spawnPos, _side, _count] call FUNC(spawnGroup);

_group addWaypoint [_targetPos, 0];
_group

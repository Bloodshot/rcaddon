#include "..\script_component.hpp"
/*
 * Reduce nearby rc_mf_snowEffect particles when under a roof.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_snowRoofHandler
 *
 * Caller: Client
 */

([positionCameraToWorld [0, 0, 0], player] call FUNC(underRoof)) params ["_underRoof", "_house"];
if (_underRoof) then {
  private _houseBounds = selectMax (boundingBox _house select 1);
  GVAR(snowEffect) setParticleCircle [_houseBounds + 17, [0, 0, 0]];
  GVAR(snowEffect) setParticleRandom [1.5, [15, 15, -1], [0, 0, 0], 0, 1, [0, 0, 0, 0.4], 0, 1, 0, 1];
  GVAR(snowEffect) setDropInterval ((1 / GVAR(snowIntensity)) * 0.1);
} else {
  GVAR(snowEffect) setParticleCircle [25, [0, 0, 0]];
  GVAR(snowEffect) setParticleRandom [1.5, [25, 25, 3], [0, 0, 0], 0, 0.035, [0, 0, 0, 0.4], 0, 1, 0, 1];
  GVAR(snowEffect) setDropInterval (1 / GVAR(snowIntensity));
};

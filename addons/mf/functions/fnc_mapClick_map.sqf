#include "..\script_component.hpp"
/*
 * Handle rc_mf_fnc_mapClick on the main map
 *
 * Caller: Client
 */

params ["_code", "_args", ["_mode", "SINGLE"], ["_color", [0, 0, 0, 1]], ["_markerColor", "ColorBLACK"]];

GVAR(mapClick_posArray) = [];
GVAR(mapClick_markers) = [];

findDisplay 49 closeDisplay 1; // Close esc menu
openMap [true, false];

private _eh = addMissionEventHandler [
  "MapSingleClick",
  {
    params ["_unit", "_pos", "_alt", "_shift"];
    private _shouldStop = [_pos, _thisArgs, _shift, false, _alt] call FUNC(mapClick_onClick);
    if (_shouldStop) exitWith {
      openMap [false, false];
    };
  },
  [_mode, _markerColor, _code, _args]
];

[
  { !visibleMap },
  {
    removeMissionEventHandler ["MapSingleClick", _this select 0];
    { deleteMarkerLocal _x } forEach GVAR(mapClick_markers);
  },
  [_eh]
] call CBA_fnc_waitUntilAndExecute;

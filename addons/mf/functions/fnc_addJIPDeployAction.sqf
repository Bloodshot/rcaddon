#include "..\script_component.hpp"
/*
 * Add a deploy action to the current player if they've JIPed after the mission
 * has started. This action will teleport them to their leader, or, if they are
 * the leader of their group, teleport them and anyone else in the homeArea to
 * any position they choose.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_addJIPDeployAction
 *
 * Caller: Client
 */

private _actionID = player addAction [
  "<t color='#ee6666'>Deploy</t>",
  { [true] call FUNC(doDeploy) },
  [],
  2,
  false,
  true,
  "",
  '[] call FUNC(canDeploy)'
];

player setVariable ["deployActionID", _actionID];

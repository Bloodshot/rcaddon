#include "..\script_component.hpp"
/*
 * Crate a plane to paradrop a supply box (see rc_mf_fnc_doParadrop,
 * rc_mf_fnc_spawnSupplyBox).
 *
 * Arguments:
 * 0: Destination position in Position 2D <ARRAY>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [markerPos "supplyParadrop"] call rc_mf_fnc_spawnSupplyDrop
 *
 * Caller: Server
 */

params ["_pos"];

_pos set [2, 500];

([_pos] call FUNC(createPlane)) params ["_plane", "_pilot"];

[_pos, _plane, _pilot] spawn {
  params ["_pos", "_plane", "_pilot"];

  private _distance = _plane distance2D _pos;
  waitUntil {
    sleep 1;

    private _lastDistance = _distance;
    _distance = _plane distance2D _pos;

    // Wait until plane starts getting farther away
    (_distance < 800) && { _distance > _lastDistance }
  };

  private _chute = createVehicle ["B_Parachute_02_F", [100, 100, 200], [], 0, "FLY"];
  _chute setPosASL ((getPosASL _plane) vectorAdd [0, 0, -20]);

  private _box = [getPosATL _chute] call FUNC(spawnSupplyBox);
  _box attachTo [_chute, [0, 0, 0]];

  sleep 120;
  deleteVehicle _pilot;
  deleteVehicle _plane;
};

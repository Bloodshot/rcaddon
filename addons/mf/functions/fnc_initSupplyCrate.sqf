#include "..\script_component.hpp"
/*
 * Initialize the inventory and ace_cargo parameters of a supply crate.
 *
 * If the crate type is given as a string, a crate with that type will be
 * searched for in the crateMap config variable. If it is a number, it is
 * interpreted as an index.
 *
 * Arguments:
 * 0: Supply crate to initialize <OBJECT>
 * 1: Crate type in the crateMap <STRING,NUMBER>
 *
 * Return:
 * Supply crate
 *
 * Example:
 * [myBox, "MMG"] call rc_mf_fnc_initSupplyCrate
 *
 * Caller: Client / Server
 */

params ["_box", "_type"];

if (isNil "crateMap") exitWith {
  ["crateMap not defined"] call BIS_fnc_error;
};

private _idx = _type;
if (typeName _type == "STRING") then {
  _idx = crateMap find _type;
};

if (isNil "_idx" || {_idx == -1}) exitWith {};

private _crate = GVAR(crates) select _idx;

_box setVariable ["ace_cargo_size", 1, true];
[_box, _crate select 0, _crate select 2] remoteExec [QFUNC(initSupplyCrateLocal), 0];
_box

#include "..\script_component.hpp"
/*
 * Enable all functionality for the respawn spectator
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_initRespawnSpectator
 *
 * Caller: Client
 */

BIS_respSpecAi                   = true;
BIS_respSpecAllowFreeCamera      = true;
BIS_respSpecAllow3PPCamera       = true;
BIS_respSpecShowFocus            = true;
BIS_respSpecShowCameraButtons    = true;
BIS_respSpecShowControlsHelper   = true;
BIS_respSpecShowHeader           = true;
BIS_respSpecLists                = true;

BIS_EGSpectator_allowAISwitch    = true;
BIS_EGSpectator_whitelistedSides = [];

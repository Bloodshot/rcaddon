#include "..\script_component.hpp"
/*
 * Converts a distance in meters to a string representing the distance using
 * variable precision. Distances less than 100m will show one digit after the
 * decimal, distances between 100 and 1000m will be shown in whole meters,
 * distances between 1km and 100km will be shown with one digit after the
 * decimal (in km), and distances 100km or bigger will be shown in whole
 * kilometers.
 *
 * Arguments:
 * 0: Distance in meters <NUMBER>
 *
 * Return:
 * Human readable string representing the distance <STRING>
 *
 * Example:
 * [52.3]   call rc_mf_fnc_humanDistance // 52.3m
 * [520.3]  call rc_mf_fnc_humanDistance // 520m
 * [2520.3] call rc_mf_fnc_humanDistance // 2.5km
 *
 * Caller: Client / Server
 */

params ["_distance"];

switch (true) do {
  case (_distance < 100): {
    private _str = str (round (_distance * 10));
    _str = _str insert [count _str - 1, "."];
    _str + " m";
  };
  case (_distance < 1000): {
    str (round _distance) + " m";
  };
  case (_distance < 100000): {
    private _str = str (round (_distance / 100));
    _str = _str insert [count _str - 1, "."];
    _str + " km";
  };
  default {
    str (round (_distance / 1000)) + " km";
  };
};

#include "..\script_component.hpp"
/*
 * Handle the Deploy addAction. The player may select a position to deploy to on
 * the map if appropriate.
 *
 * This function sends a deploy event to the server, which should trigger a
 * deploy.
 *
 * Arguments:
 * 0: Whether this is a JIP deploy (default: true if the mission has
 *    started) <BOOLEAN>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_doDeploy
 *
 * Caller: Client
 */

params [["_jip", nil]];

if (isNil "_jip") then {
  _jip = missionNamespace getVariable [QGVAR(missionStarted), false];
};

if (player == leader group player) then {
  if (!_jip && deployDestination == "marker" && {
    !isNil { [group player] call FUNC(getDeployMarker) }
  }) then {
    [QGVAR(deploy), [player]] call CBA_fnc_serverEvent;
  } else {
    private _event = [QGVAR(deploy), QGVAR(deployJIP)] select _jip;
    openMap [true, false];
    private _eh = addMissionEventHandler [
      "MapSingleClick",
      {
        params ["_units", "_pos", "_alt", "_shift"];
        _thisArgs params ["_event"];
        openMap [false, false];
        [_event, [player, _pos]] call CBA_fnc_serverEvent;
      },
      [_event]
    ];

    [
      { !visibleMap },
      { removeMissionEventHandler ["MapSingleClick", _this] },
      _eh
    ] call CBA_fnc_waitUntilAndExecute;
  };
} else {
  [QGVAR(deployJIP), [player, getPosATL (leader group player)]] call CBA_fnc_serverEvent;
};

#include "..\script_component.hpp"
/*
 * Add crates definitions to the framework's cratePool
 *
 * Arguments:
 * 0: List of crate definitions <ARRAY>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [[] call compile loadFile "crates.sqf"] call rc_mf_fnc_addCrates
 *
 * Caller: Client / Server
 */

params ["_crates"];

if (isNil QGVAR(crates)) then {
  GVAR(crates) = [];
};

{
  GVAR(crates) pushBackUnique _x;
} forEach _crates;

#include "..\script_component.hpp"
/*
 * Ensure all units have their radio and voice chatter muted.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_muteAllUnits
 *
 * Caller: Server
 */

["Man", "Init", {
  [(_this select 0), ""] call ace_common_fnc_muteUnit;
}, true, [], true] call CBA_fnc_addClassEventHandler;

#include "..\script_component.hpp"
/*
 * Selects one player who is a leader and calls addDeployAction on them. Groups
 * are searched in the order they're defined (in mission.sqm) or created.
 *
 * Used for deployAction = "once", which deploys everyone.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_addDeployActionToTopLeader
 *
 * Caller: Server
 */

if (!isServer) exitWith {};

private _friendlySides = missionNamespace getVariable ["friendlySides", [west]];

{
  if (side _x in _friendlySides && {
    count (units _x) > 0
  } && {
    isPlayer (leader _x)
  }) then {
    [] remoteExec [QFUNC(addDeployAction), leader _x];
    break;
  };
} forEach allGroups;

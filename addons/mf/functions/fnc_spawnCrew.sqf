#include "..\script_component.hpp"
/*
 * Spawn crew of the provided side's crew template and move them into crew
 * positions in a vehicle.
 *
 * Arguments:
 * 0: Vehicle to crew <VEHICLE>
 * 1: Side from which to spawn crew units (default: enemySide, or east) <SIDE>
 *
 * Return:
 * Spawned group <GROUP>
 *
 * Example:
 * [apc, west] call rc_mf_fnc_spawnCrew
 *
 * Caller: Server
 */

params ["_veh", ["_side", missionNamespace getVariable ["enemySide", east]]];

private _tempGroup = createGroup _side;
private _grp = createGroup _side;
_grp addVehicle _veh;

private _unarmored = getNumber (configOf _veh >> "crewVulnerable") == 1;
private _groupTemplates = GVAR(groupTemplates) select (_side call BIS_fnc_sideID);
private _crewTemplates = GVAR(crewTemplates) select (_side call BIS_fnc_sideID);

private _groupTemplate = if (_unarmored) then {
  selectRandom _groupTemplates;
} else {
  selectRandom ([_crewTemplates, _groupTemplates] select (_crewTemplates isEqualTo []));
};
if (isNil "_groupTemplate") exitWith {};

private _seats = ([_veh] call BIS_fnc_vehicleCrewTurrets);

for "_i" from 1 to (count _seats) do {
  (selectRandom _groupTemplate) params ["_class", "_loadout"];
  _crew = _tempGroup createUnit [_class, [0, 0, 0], [], 0, "NONE"];
  [_crew] call FUNC(addDispersionEH);

  if (count _loadout > 0) then {
    _crew setUnitLoadout _loadout;
  };
};

(units _tempGroup) joinSilent _grp;
deleteGroup _tempGroup;

{
  private _seat = _seats select _forEachIndex;
  if (_seat isEqualTo [-1]) then {
    _x moveInDriver _veh;
  } else {
    _x moveInTurret [_veh, _seat];
  };
} forEach (units _grp);

addToRemainsCollector (units _grp);

_grp

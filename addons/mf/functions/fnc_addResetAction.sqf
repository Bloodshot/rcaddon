#include "..\script_component.hpp"
/*
 * Add an action to the current player that resets their loadout to its initial
 * state, available before deployment / mission start.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_addResetAction
 *
 * Caller: Client
 */

private _actionID = player addAction [
  "Reset Loadout",
  { player setUnitLoadout (_this select 3 select 0) },
  [getUnitLoadout player],
  4,
  false,
  true,
  "",
  '[] call FUNC(canOpenArsenal)'
];

player setVariable ["resetActionID", _actionID];

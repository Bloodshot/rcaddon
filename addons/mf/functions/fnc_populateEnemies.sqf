#include "..\script_component.hpp"
/*
 * Populate the map with various units based on editor placed markers.
 *
 * The following 3DEN Layers are searched for markers:
 *   "{SIDE} Garrisons", "Garrisons"
 *   "{SIDE} Patrols", "Patrols"
 *   "{SIDE} Guards", "Guards"
 *   "{SIDE} Town Garrisons", "Town Garrisons"
 * Where {SIDE} is one of "Blufor", "Opfor", "Indep".
 * Layers without a side prefix are taken to be enemySide, or east if enemySide
 * is not defined.
 *
 * Arguments:
 * 0: arg <TYPE>
 *
 * Return:
 * Return value <TYPE>
 *
 * Example:
 * [] call rc_mf_fnc_populateEnemies
 *
 * Caller: Server
 */

{
  _x params ["_layerPrefix", "_side"];

  [format ["%1Garrisons", _layerPrefix], {
    [_this, 30, _side] call FUNC(garrisonNearby);
  }] call FUNC(eachMarker);

  [format ["%1Patrols", _layerPrefix], {
    [_this, _side, 8] call FUNC(spawnPatrol);
  }] call FUNC(eachMarker);

  [format ["%1Guards", _layerPrefix], {
    [_this, _side, selectRandom [1, 2]] call FUNC(spawnGuards);
  }] call FUNC(eachMarker);

  if (count (getMissionLayerEntities format ["%1Town Garrisons", _layerPrefix]) > 1) then {
    {
      private _radius = markerSize _x select 0;
      if (_radius == 1) then { _radius = 250 };
      private _buildingsToGarrison = nearestObjects [markerPos _x, ["Building"], _radius];
      private _numBuildings = {(_x buildingPos 0) isNotEqualTo [0, 0, 0]} count _buildingsToGarrison;
      private _numUnits = round  (_numBuildings * 3 * (missionNamespace getVariable ["buildingGarrisonRatio", 1 / 5]));
      [_buildingsToGarrison, _numUnits, _side] call FUNC(garrisonBuildings);
    } forEach (getMissionLayerEntities format ["%1Town Garrisons", _layerPrefix] select 1);
  };
} forEach [
  ["", missionNamespace getVariable ["enemySide", east]],
  ["OPFOR ", east],
  ["BLUFOR ", west],
  ["INDEP ", independent]
];

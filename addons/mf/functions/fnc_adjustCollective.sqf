#include "..\script_component.hpp"
/*
 * Instantly adjust the collective pitch of the player's helicopter by the given
 * amount. Does nothing if the player is not in an RTD aircraft.
 *
 * Arguments:
 * 0: Collective amount, as a percentage <NUMBER>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [-0.07] call rc_mf_fnc_adjustCollective
 *
 * Caller: Client
 */

// Todo: move out of mission framework

params ["_amount"];

private _veh = vehicle player;
if (isObjectRTD _veh && { driver _veh == player }) then {
  _veh setActualCollectiveRTD (collectiveRTD _veh + _amount);
};

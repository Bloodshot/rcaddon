#include "..\script_component.hpp"
/*
 * Display a countdown timer on the right side of the screen, disappearing some
 * time after hitting 0.
 *
 * Arguments:
 * 0: Start time of the timer, should be obtained from serverTime <NUMBER>
 * 1: Duration of the timer, in seconds <NUMBER>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [serverTime, 300] remoteExec ["rc_mf_fnc_displayCountdown", rc_mf_allClients];
 *
 * Caller: Client
 */

params ["_startTime", "_duration"];

if (serverTime > (_startTime + _duration)) exitWith {};

RscFiringDrillTime_done = false;
1 cutRsc ["RscFiringDrillTime", "PLAIN"];

[{
  (_this select 0) params ["_endTime"];

  if (serverTime >= _endTime || {
    missionNamespace getVariable ["RscFiringDrillTime_done", false]
  }) exitWith {
    RscFiringDrillTime_current = parseText "<t align='left' color='#FAFAFA'>00:00.000";
    [_this select 1] call CBA_fnc_removePerFrameHandler;

    [{ RscFiringDrillTime_done = true }, [], 4] call CBA_fnc_waitAndExecute;
  };

  private _timeFormat = [_endTime - serverTime, "MM:SS.MS", true] call BIS_fnc_secondsToString;

  private _text = format ["<t align='left' color='#FAFAFA'>%1:%2.%3",
    _timeFormat select 0,
    _timeFormat select 1,
    _timeFormat select 2
  ];
  RscFiringDrillTime_current = parseText _text;
}, 0, [_startTime + _duration]] call CBA_fnc_addPerFrameHandler;

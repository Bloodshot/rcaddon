#include "..\script_component.hpp"
/*
 * Garrison a random subset of the provided buildings with units spawned of the
 * provided side. Units will be placed and fixed to buildingPos's of the
 * selected buildings. Small groups will tend to spawn, but they may be as large
 * as the number of building positions in the building, or the provided maximum
 * (whichever is smaller).
 *
 * Buildings will be garrisoned until the provided unit count is met.
 *
 * Arguments:
 * 0: Buildings to potentially garrison <ARRAY>
 * 1: Maximum number of units to spawn <NUMBER>
 * 2: Side of the spawned units (default: enemySide, or east) <SIDE>
 * 3: Spawn function to use to spawn units (default: rc_mf_fnc_spawnGroup) <CODE>
 * 4: Maximum number of units per building (default: 100) <NUMBER>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [bunkers, 40, east] call rc_mf_fnc_garrisonBuildings
 *
 * Caller: Server
 */

params ["_buildings", "_unitCount", ["_side", missionNamespace getVariable ["enemySide", east]], ["_spawnFunction", FUNC(spawnGroup)], ["_maxPer", 100]];

if (count _buildings == 0) exitWith {};

_buildings = [_buildings] call CBA_fnc_shuffle;

private _buildingPositions = _buildings apply {
  private _positions = GVAR(buildingPositions) get (typeOf _x);

  if (!isNil "_positions") then {
    private _buildingAzimuth = getDir _x;
    private _buildingPos = getPosATL _x;
    _positions apply {
      ([_x, -_buildingAzimuth] call BIS_fnc_rotateVector2D) vectorAdd _buildingPos
    };
  } else {
    _x buildingPos -1
  }
} select {
  count _x > 0
};

private _unitArray = [];
private _buildingIndex = 0;

while { _unitCount > 0 } do {
  if (_buildingIndex >= count _buildingPositions) exitWith {};
  private _building = [(_buildingPositions select _buildingIndex), true] call CBA_fnc_shuffle;
  _buildingIndex = _buildingIndex + 1;

  // Spawn a random number of units up to the maximum that'll fit
  private _numUnits = round (
    random [2 min _maxPer min (count _building), _maxPer min 4 min (count _building), _maxPer min (count _building)]
  );
  _numUnits = _numUnits min _unitCount;
  _unitCount = _unitCount - _numUnits;

  while { count _unitArray < _numUnits } do {
    private _newGroup = [[0, 0, 0], _side, _numUnits] call _spawnFunction;
    if (isNil "_newGroup") exitWith {
      ["Could not spawn group to garrison buildings"] call BIS_fnc_error;
    };
    _unitArray append (units _newGroup);
  };

  // Error path
  if (count _unitArray == 0) exitWith {};

  private _garrisonGroup = createGroup _side;

  for "_i" from 0 to (_numUnits - 1) do {
    private _unit = _unitArray select _i;
    private _pos = _building select _i;
    private _posSurface = surfaceIsWater _pos;

    if (_posSurface) then {
        _unit setPosASL (AGLtoASL _pos);
    } else {
        _unit setPosATL _pos;
    };

    private _unitPos = "MIDDLE";

    if ([_unit] call FUNC(underRoof) select 0) then {
      _unitPos = "UP";
    };

    _unit setUnitPos _unitPos;

    [_unit] joinSilent _garrisonGroup;
    [_unit] call FUNC(holdPosition);

    _unit setUnitPos _unitPos;
    _unit setVariable ["unitPos", _unitPos];
  };

  _unitArray deleteRange [0, _numUnits];
};

{
  private _grp = group _x;
  deleteVehicle _x;
  if (count (units _grp) == 0) then {
    deleteGroup _grp;
  };
} forEach _unitArray;

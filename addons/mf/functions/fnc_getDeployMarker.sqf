#include "..\script_component.hpp"
/*
 * Get the name of the marker appropriate for deploying the passed in group, or
 * nil if none is found. Used for deployDestination = "marker".
 *
 * Searches for the following, where groupName is the group's ID with spaces
 * replaced by underscores, and groupSideName is one of "Opfor", "Blufor",
 * "Indep", "Civilian":
 *   "deployMarker" + groupName
 *   groupName + "Deploy"
 *   "deployMarker" + groupSideName
 *   "deployMarker"
 *
 * Arguments:
 * 0: Group to find a deploy marker for <GROUP>
 *
 * Return:
 * Marker name, or nil <STRING>
 *
 * Example:
 * [group player] call rc_mf_fnc_getDeployMarker
 *
 * Caller: Client / Server
 */

params ["_group"];

private _groupID = (groupID _group) regexReplace [" ", "_"];

private _marker = (format ["%1Deploy", _groupID]);
if ((markerPos _marker) isNotEqualTo [0, 0, 0]) exitWith { _marker };

private _marker = (format ["deployMarker%1", _groupID]);
if ((markerPos _marker) isNotEqualTo [0, 0, 0]) exitWith { _marker };

_marker = format ["deployMarker%1", side _group];
if ((markerPos _marker) isNotEqualTo [0, 0, 0]) exitWith { _marker };

private _sideName = ["Opfor", "Blufor", "Indep", "Civilian"] select ((side _group) call BIS_fnc_sideID);
_marker = format ["deployMarker%1", _sideName];
if ((markerPos _marker) isNotEqualTo [0, 0, 0]) exitWith { _marker };

_marker = "deployMarker";
if ((markerPos _marker) isNotEqualTo [0, 0, 0]) exitWith { _marker };

nil

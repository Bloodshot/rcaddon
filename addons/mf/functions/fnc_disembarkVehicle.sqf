#include "..\script_component.hpp"
/*
 * Have the AI quickly disembark a land vehicle, rather than one by one
 *
 * Arguments:
 * 0: Land vehicle to disembark <VEHICLE>
 * 1: Group of units to disembark from vehicle (default: everyone in vehicle) <GROUP>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [troopTruck, embarkedGroup] call rc_mf_fnc_disembarkVehicle
 *
 * Caller: Server
 */

params ["_vic", ["_group", nil]];

private _units = if (isNil "_group") then {
  units _vic
} else {
  (units _group) select { vehicle _x == _vic };
};

{
  unassignVehicle _x;
} forEach _units;

waitUntil { speed _vic < 5 };

{
  _x moveOut (vehicle _x);
  sleep random [0.25, 0.5, 0.8];
} forEach _units;

#include "..\script_component.hpp"
/*
 * Find the player nearest to a position
 *
 * Arguments:
 * 0: Position to search around <POSITION>
 *
 * Return:
 * Player <UNIT>
 *
 * Example:
 * [markerPos "base"] call rc_mf_fnc_nearestPlayer
 *
 * Caller: Client / Server
 */

params ["_pos"];

_pos = if (typeName _pos != "ARRAY") then {
  getPosASL _pos
} else {
  _pos
};

_closestPlayer = objNull;
_bestDistance = 1E10;
{
  if (alive _x && { side _x in missionNamespace getVariable ["friendlySides", [side _x]] }) then {
    private _dsqr = _pos distanceSqr (getPosASL _x);
    if (_dsqr < _bestDistance) then {
      _closestPlayer = _x;
      _bestDistance = _dsqr;
    };
  };
} forEach allPlayers;

_closestPlayer

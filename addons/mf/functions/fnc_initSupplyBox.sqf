#include "..\script_component.hpp"
/*
 * Initializes a supply box (a large box containing multiple supply crates),
 * spawning supply crates based on the supplyDropCrates config variable.
 *
 * Arguments:
 * 0: Supply box <OBJECT>
 *
 * Return:
 * Supply box
 *
 * Example:
 * [supplyBox] call rc_mf_fnc_initSupplyBox
 *
 * Caller: Client / Server
 */

params ["_box"];

if (isNil "supplyDropCrates") exitWith {
  ["supplyDropCrates not set"] call BIS_fnc_error;
};

[_box] call FUNC(clearCargo);
[_box, 8] call ace_cargo_fnc_setSpace;

[
  {
    typeName ((_this select 0) getVariable ["ace_cargo_space", objNull]) == "SCALAR"
  },
  {
    params ["_box"];
    {
      private _crate = [_x, [0, 0, 0]] call FUNC(spawnSupplyCrate);
      [_crate, _box, true] call ace_cargo_fnc_loadItem;
    } forEach supplyDropCrates;
  },
  [_box]
] call CBA_fnc_waitUntilAndExecute;

_box

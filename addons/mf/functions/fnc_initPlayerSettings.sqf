#include "..\script_component.hpp"
/*
 * Initialize various unrelated settings and variables on players
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_initPlayerSettings
 *
 * Caller: Client
 */

setViewDistance 5000;
setPlayerRespawnTime 1E12;
[player, 0, true] call BIS_fnc_respawnTickets;
removeFromRemainsCollector [player];
player setVariable [QGVAR(initialLoadout), getUnitLoadout player, true];
player setVariable [QGVAR(initialPos), getPosASL player, true];

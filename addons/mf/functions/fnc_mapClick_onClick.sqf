#include "..\script_component.hpp"
/*
 * Handle calling the callback for rc_mf_fnc_mapClick after a click event
 *
 * Caller: Client
 */

params ["_pos", "_args", "_shift", "_ctrl", "_alt"];
_args params ["_mode", "_markerColor", "_code", "_codeArgs"];

if (_mode == "DOUBLE" || _mode == "PAIRS") then {
  GVAR(mapClick_posArray) pushBack _pos;
  private _return = false;

  if (count GVAR(mapClick_posArray) > 1) then {
    [GVAR(mapClick_posArray) select 0, GVAR(mapClick_posArray) select 1, _codeArgs, _shift, _alt] call _code;
    GVAR(mapClick_posArray) resize 0;
    _markerColor = "ColorBLACK";
    _return = (_mode == "DOUBLE");
  };

  private _marker = createMarkerLocal [format [QGVAR(mapClickMarker%1), count GVAR(mapClick_markers)], _pos];
  _marker setMarkerTypeLocal "mil_dot";
  _marker setMarkerColorLocal _markerColor;
  GVAR(mapClick_markers) pushBack _marker;

  _return
} else {
  private _callbackResult = [_pos, _codeArgs, _shift, _alt] call _code;

  if (_mode == "MANY" || { _mode == "SCRIPTED" && { !_callbackResult } }) then {
    private _marker = createMarkerLocal [format [QGVAR(mapClickMarker%1), count GVAR(mapClick_markers)], _pos];
    _marker setMarkerTypeLocal "mil_dot";
    _marker setMarkerColorLocal _markerColor;
    GVAR(mapClick_markers) pushBack _marker;

    false
  } else {
    true
  };
};

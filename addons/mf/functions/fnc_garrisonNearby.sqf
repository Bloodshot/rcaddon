#include "..\script_component.hpp"
/*
 * Find nearby buildings and garrison them. Then spawn a group of guards to
 * defend around the provided position.
 *
 * Arguments:
 * 0: Position to garrison around in Position AGL <ARRAY>
 * 1: Radius in meters to search for buildings <NUMBER>
 * 2: Side of the spawned units <SIDE>
 * 3: The number of units to spawn in garrisons (default: 6) <NUMBER>
 * 4: The number of units to spawn as defenders (default: 4) <NUMBER>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [markerPos "friendlyBunker", 50, west, 20, 5] call rc_mf_fnc_garrisonNearby
 *
 * Caller: Server
 */

params ["_pos", "_range", "_side", ["_count", 6], ["_guardCount", 4]];

private _buildingsToGarrison = nearestObjects [_pos, ["Building"], _range];
[_buildingsToGarrison, _count, _side] call FUNC(garrisonBuildings);

if (_guardCount > 0) then {
  while { _guardCount > 0 } do {
    private _guardPos = [_pos, 0, _range, 3] call BIS_fnc_findSafePos;
    [_pos, _side, _guardCount min 4] call FUNC(spawnGuards);
    _guardCount = _guardCount - 4;
  };
};

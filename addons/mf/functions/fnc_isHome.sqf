#include "..\script_component.hpp"
/*
 * Determines whether or not an object is considered to be in the homeArea,
 * enabling the use of certain pre mission actions (and a few others).
 *
 * Arguments:
 * 0: Object to test <OBJECT>
 *
 * Return:
 * Whether the object is in the homeArea, true if homeArea is missing <BOOLEAN>
 *
 * Example:
 * [player] call rc_mf_fnc_isHome
 *
 * Caller: Client / Server
 */

params ["_obj"];

if (markerShape "homeArea" == "") exitWith {};
_obj inArea "homeArea";

#include "..\script_component.hpp"
/*
 * Determine whether another player should be tracked using BFT.
 *
 * Arguments:
 * 0: Other player <UNIT>
 *
 * Return:
 * Whether to track the provided player using BFT <BOOLEAN>
 *
 * Example:
 * [leader alpha1] call rc_mf_fnc_shouldBFTTrack
 *
 * Caller: Client
 */

params ["_player"];

isPlayer _player && {
  _player != player
} && {
  side _player == side player
} && {
  (vehicle _player) isKindOf "Helicopter"
} && {
  [] call FUNC(shouldTrackWaypoint)
} && {
  vehicle player != vehicle _player
} && {
  _player == leader (group _player)
};

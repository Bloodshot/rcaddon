#include "..\script_component.hpp"
/*
 * Deprecated
 *
 * Caller: Server
 */

if (isNil "crateLoader") exitWith {};
if (isNil "ace_cargo_fnc_loadItem") exitWith {};

_targets = [];

{
  private _crateGroup = _x;
  private _groupLinks = (synchronizedObjects _crateGroup);
  private _targetLogicIndex =  _groupLinks findIf {_x isKindOf "Logic" && {_x != crateLoader}};

  if (_targetLogicIndex != -1) then {
    private _targetLogic = _groupLinks select _targetLogicIndex;
    private _crates = _groupLinks - [_targetLogic, crateLoader];
    private _targetLinks = synchronizedObjects _targetLogic;
    private _target = _targetLinks select (_targetLinks findIf {!(_x isKindOf "Logic")});
    {
      private _crate = _x;
      [_crate, _target, true] call ace_cargo_fnc_loadItem;
    } forEach _crates;

    _targets pushBackUnique _target;
  };
} forEach synchronizedObjects crateLoader;

["ace_cargoUnloaded", {
  params ["_item", "_vehicle"];
  _thisArgs params ["_targets"];

  if (!(_vehicle in _targets)) exitWith {};

  if (count (_vehicle getVariable ["ace_cargo_loaded", -1]) == 0) then {
    deleteVehicle _vehicle;
  };
}, [_targets]] call CBA_fnc_addEventHandlerArgs;

#include "..\script_component.hpp"
/*
 * Handle the Open Arsenal action, opening ACE Arsenal, and running
 * rc_mf_fnc_limitMedical when it is closed.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_openArsenal
 *
 * Caller: Client
 */

if (isNil { player getVariable "ace_arsenal_virtualItems" }) then {
  [] call FUNC(addArsenalItems);
};

[player, player] call ace_arsenal_fnc_openBox;

[
  {
    !isNull (missionNamespace getVariable [ "ace_arsenal_camera", objNull ])
  },
  {
    [
      {
        isNull (missionNamespace getVariable [ "ace_arsenal_camera", objNull ])
      },
      FUNC(limitMedical)
    ] call CBA_fnc_waitUntilAndExecute;
  }
] call CBA_fnc_waitUntilAndExecute;

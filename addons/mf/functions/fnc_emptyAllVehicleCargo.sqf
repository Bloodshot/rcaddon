#include "..\script_component.hpp"
/*
 * Ensure that all vehicles spawn with empty inventories
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_emptyAllVehicleCargo
 *
 * Caller: Server
 */

["AllVehicles", "Init", {
  [(_this select 0)] call FUNC(clearCargo);
}, true, [], true] call CBA_fnc_addClassEventHandler;

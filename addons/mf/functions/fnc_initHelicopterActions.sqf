#include "..\script_component.hpp"
/*
 * Add repair and supply crate spawning actions to a helicopter, visible when
 * the aircraft is in homeArea.
 *
 * Also adds a toolkit and small fuel can to the helicopter's cargo.
 *
 * Arguments:
 * 0: Helicopter to add actions to <VEHICLE>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [transportHelicopter] call rc_mf_fnc_initHelicopterActions
 *
 * Caller: Server
 */
params ["_helo"];

[_helo] call FUNC(clearCargo);

[_helo] call FUNC(addCrateActions);

[_helo,
  [
    "Repair, Refuel, and Rearm",
    {
      _target setDamage 0;
      _target setFuel 1;
      _target setVehicleAmmo 1;
    },
    nil,
    2,
    true,
    false,
    "",
    '[_target] call FUNC(canRepairAircraft)',
    8
  ]
] remoteExec ["addAction", 0, true];

(_helo) addItemCargoGlobal ["Toolkit", 1];

private _can = "Land_CanisterFuel_F" createVehicle [0, 0, 0];
[_can, _helo, true] call ace_cargo_fnc_loadItem;

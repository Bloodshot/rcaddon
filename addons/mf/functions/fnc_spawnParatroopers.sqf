#include "..\script_component.hpp"
/*
 * Spawn a plane to drop paratroopers around the destination.
 *
 * Arguments:
 * 0: Destination position in Position 2D <ARRAY>
 * 1: Side of units to spawn (default: enemySide, or east) <SIDE>
 * 2: Number of units to spawn, -1 to fill plane (default: -1) <NUMBER>
 * 3: Class of plane to use (default: enemyParatrooperVehicle) <STRING>
 * 4: Spawn position of plane (default: markerPos "planeStart") <ARRAY>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [markerPos "paradropLocation", west, 10] call rc_mf_fnc_spawnParatroopers
 *
 * Caller: Server
 */

params [
  "_dest",
 ["_side", missionNamespace getVariable ["enemySide", east]],
 ["_count", -1],
 ["_paratrooperVehicle", enemyParatrooperVehicle],
 ["_spawnPos", markerPos "planeStart"]
];

if (isNil "_paratrooperVehicle") exitWith {
  ["No enemyParatrooperVehicle defined"] call BIS_fnc_error;
};

private _groupTemplate = selectRandom (GVAR(crewTemplates) select (_side call BIS_fnc_sideID));
if (isNil "_groupTemplate") exitWith {};
(selectRandom _groupTemplate) params ["_class", "_loadout"];
private _pilot = (createGroup _side) createUnit [_class, [0, 0, 0], [], 0, "NONE"];
[_pilot] joinSilent (createGroup _side);

if (count _loadout > 0) then {
  _pilot setUnitLoadout _loadout;
};

([_dest, _paratrooperVehicle, _spawnPos, _pilot] call FUNC(createPlane)) params ["_plane"];

[_plane, true, _side, _count] call FUNC(fillTransportSlots);

[_dest, _plane, _pilot] spawn FUNC(paradropTracker);

#include "..\script_component.hpp"
/*
 * Call rc_mf_fnc_addWaypointTracker for the main map and GPS.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_initWaypointTracker
 *
 * Caller: Client
 */

// GPS (on-foot)
private _controls = ((uiNamespace getVariable "IGUI_DISPLAYS") select {
  ctrlIDD _x == 311
} apply {
  _x displayCtrl 101
});

{ _x call FUNC(addWaypointTracker) } forEach _controls;

#include "..\script_component.hpp"
/*
 * Remove all "home" actions on the player. Called after the player has deployed
 * or the mission has begun.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_removeHomeActions
 *
 * Caller: Client
 */

if (!isNil { player getVariable ["deployActionID", nil] }) then {
  player removeAction (player getVariable "deployActionID");
  player setVariable ["deployActionID", nil];
};

if (!isNil { player getVariable "arsenalActionID" }) then {
  player removeAction (player getVariable "arsenalActionID");
  player setVariable ["arsenalActionID", nil];
};

if (!isNil { player getVariable "resetActionID" }) then {
  player removeAction (player getVariable "resetActionID");
  player setVariable ["resetActionID", nil];
};

#include "..\script_component.hpp"
/*
 * Special logic for enemy vehicles with top-guns. Switches the driver to the
 * gunner seat if the gunner is killed.
 *
 * Arguments:
 * 0: Vehicle to watch <VEHICLE>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [technical] call rc_mf_fnc_watchAV
 *
 * Caller: Server
 */

params ["_vic"];

while { !isNull _vic } do {
  private _gunner = gunner _vic;
  private _driver = driver _vic;
  if (alive _gunner) then { sleep 5 ; continue };
  sleep 5;
  if (!alive _driver) exitWith {};

  moveOut _gunner;
  moveOut _driver;

  waitUntil { isNull (objectParent _driver) && isNull (objectParent _gunner) };

  _driver assignAsGunner _vic;

  _time = time;
  waitUntil {
    _driver moveInGunner _vic;
    (objectParent _driver) == _vic || {
      time - _time > 5;
    };
  };
};

#include "..\script_component.hpp"
/*
 * Enable dynamic simulation for the provided groups
 *
 * Arguments:
 * 0: List of groups <ARRAY>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [allGroups] call rc_mf_fnc_initDynamicSimulationLocal
 *
 * Caller: Client / Server
 */

params ["_groups"];

{
  // Skip groups with frozen/disabled units
  if ((units _x) findIf { !simulationEnabled _x } >= 0) then { continue };

  _x enableDynamicSimulation true;
  {
    _x enableDynamicSimulation true;
    _x triggerDynamicSimulation false;
  } forEach (units _x);
} forEach _groups;

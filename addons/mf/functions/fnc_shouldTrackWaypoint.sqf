#include "..\script_component.hpp"
/*
 * Whether the player's custom waypoint should be tracked on map controls.
 *
 * By default, only enabled when the player is in an air vehicle.
 *
 * Return:
 * Whether to enable waypoint tracking <BOOLEAN>
 *
 * Example:
 * [] call rc_mf_fnc_shouldTrackWaypoint
 *
 * Caller: Client
 */

(vehicle player) isKindOf "Air"

#include "..\script_component.hpp"
/*
 * Define an administrative helper function.
 * These are set outside of the rc_mf namespace, in the mission namespace, and
 * are designed to be called directly using the console by an admin
 * interactively, for the purposes of GMing or debugging.
 *
 * Arguments:
 * 0: Name of the helper <STRING>
 * 1: Helper function <CODE>
 *
 * Return:
 * Nothing
 *
 * Example:
 * ["startPhase2", {["Update", ["Phase 2 has begun!"]] remoteExec ["BIS_fnc_showNotification", 2]}] call rc_mf_fnc_defineAdminHelper
 *
 * Caller: Client
 */

params ["_name", "_code"];

// Exit if something's already using the function name
if (!isNil _name) exitWith {};

GVAR(adminHelpers) set [_name, _code];

private _helper = compile format ['["%1", _this] call FUNC(callAdminHelper);', _name];

missionNamespace setVariable [_name, _helper];

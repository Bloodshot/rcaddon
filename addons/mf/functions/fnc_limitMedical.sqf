#include "..\script_component.hpp"
/*
 * Limit a unit's medical supplies to a hardcoded maximum. Usually called after
 * a player has finished editing their loadout in the arsenal. Intended to keep
 * players from overstocking on medical supplies before a mission's start.
 *
 * Medics have higher limits than non-medics
 *
 * Arguments:
 * 0: Unit to limit the medical supplies of (default: player) <UNIT>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_limitMedical
 *
 * Caller: Client / Server
 */

params [["_unit", player]];

private _isMedic = [_unit] call ace_common_fnc_isMedic;
private _limits = [
  [
    ["ACE_fieldDressing", 10], ["ACE_splint", 3], ["ACE_morphine", 3], ["ACE_tourniquet", 2]
  ],
  [
    ["ACE_fieldDressing", 45], ["ACE_splint", 8], ["ACE_morphine", 8], ["ACE_tourniquet", 6],
    ["ACE_epinephrine", 8], ["ACE_adenosine", 8], ["ACE_personalAidKit", 8]
  ]
] select _isMedic;

private _fluidsLimit = [2500, 11000] select _isMedic;

private __unitItems = uniqueUnitItems _unit;

{
  _x params ["_class", "_max"];

  private _count = __unitItems getOrDefault [_class, 0];

  for "_i" from 1 to (_count - _max) do {
    _unit removeItem _class;
  };
} forEach _limits;

private _fluids = 0;

{
  _x params ["_class", "_volume"];
  private _count = __unitItems getOrDefault [_class, 0];
  _fluids = _fluids + _volume * _count;

  if (_fluids > _fluidsLimit) then {
    private _remove = (ceil ((_fluids - _fluidsLimit) / _volume)) min _count;
    for "_i" from 1 to _remove do {
      _unit removeItem _class;
      _fluids = _fluids - _volume;
    };
  };
} forEach [
  ["ACE_bloodIV", 1000], ["ACE_bloodIV_500", 500], ["ACE_bloodIV_250", 250],
  ["ACE_salineIV", 1000], ["ACE_salineIV_500", 500], ["ACE_salineIV_250", 250]
];

#include "..\script_component.hpp"
/*
 * Allow a player to specify one or more positions on the map by clicking on
 * them in the main map or in the zeus interface (if they are a curator),
 * calling a callback for each position or each pair of positions, one or more
 * times.
 *
 * How the callback is called is determined by the mode parameter.
 *
 * When mode is "SINGLE" or "DOUBLE", the callback is called once, with one or
 * two positions, and then the routine exits. When the mode is "MANY" or
 * "PAIRS", the callback is called for every click or pair of clicks, with one
 * or two positions, until it is cancelled (usually by ESC).
 *
 * When mode is "SCRIPTED", the callback is called for every click with one
 * position, until the callback returns false, at which point the routine exits.
 *
 * Used for GMing
 *
 * Arguments:
 * 0: Callback <CODE>
 * 1: Callback arguments <ARRAY>
 * 2: Mode <STRING>
 * 3: Color used to draw some on screen elements <ARRAY>
 * 4: Marker color used to create markers that indicate clicked positions <STRING>
 *
 * When mode is "PAIRS" or "DOUBLE", the callback is passed:
 * 0: First position
 * 1: Second position
 * 2: Callback arguments
 * 3: Whether Shift was held
 * 4: Whether Alt was held
 *
 * Otherwise, callback is passed:
 * 0: Position in Position
 * 1: Callback arguments
 * 2: Whether Shift was held
 * 3: Whether Alt was held
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_mapClick
 *
 * Caller: Client
 */

if (isNull (getAssignedCuratorLogic player)) then {
  _this call FUNC(mapClick_map);
} else {
  findDisplay 49 closeDisplay 2; // Close esc menu

  if (isNull (findDisplay 312)) then { openCuratorInterface };

  [
    { !isNull (findDisplay 312) },
    { _this call FUNC(mapClick_zeus) },
    _this,
    5
  ] call CBA_fnc_waitUntilAndExecute;
};

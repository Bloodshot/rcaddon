#include "..\script_component.hpp"
/*
 * Add group event handlers to the player's group that will update the player's
 * icon and radio channel when their membership changes.
 *
 * Arguments:
 * 0: Group to add EHs to (default: player's group) <GROUP>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_initGroupEHs
 *
 * Caller: Client
 */

params [["_group", group player]];

if (isNil { _group getVariable QGVAR(groupLeaveEH) }) then {
  private _eh = _group addEventHandler ["UnitLeft", {
    params ["_group", "_oldUnit"];

    if (_oldUnit != player) exitWith {};

    private _eh = _group getVariable QGVAR(groupLeaveEH);
    if !(isNil "_eh") then {
      _group removeEventHandler ["UnitLeft", _eh];
    };

    _eh = _group getVariable QGVAR(groupLeaderEH);
    if !(isNil "_eh") then {
      _group removeEventHandler ["LeaderChanged", _eh];
    };

    [
      { !isNull (group player) },
      {

        [group player] call FUNC(initGroupEHs);

        [] call FUNC(setRadarIcon);

        private _radioChannel = (group player) getVariable "radioChannel";
        if (!isNil "_radioChannel" && { !isNil "squadRadio" }) then {
          private _squadRadio = [squadRadio] call acre_api_fnc_getRadioByType;
          if (!isNil "_squadRadio") then {
            [_squadRadio, _radioChannel] call acre_api_fnc_setRadioChannel;
            (format ["Switched squad radio to Channel %1 automatically", _radioChannel]) call CBA_fnc_notify;
          };
        };
      }
    ] call CBA_fnc_waitUntilAndExecute;
  }];

  _group setVariable [QGVAR(groupLeaveEH), _eh];
};

if (isNil { _group getVariable QGVAR(groupLeaderEH) }) then {
  private _eh = _group addEventHandler ["LeaderChanged", {
    [] call FUNC(setRadarIcon);
  }];

  _group setVariable [QGVAR(groupLeaderEH), _eh];
};

#include "..\script_component.hpp"
/*
 * Generate an intermediate position within a distance of the target,
 * in the direction of the the source position, with a random anglular displacement.
 *
 * Useful for waypoints that have the enemy perform flanking
 *
 * Arguments:
 * 0: Source position (in any format) <ARRAY>
 * 1: Destination position (in any format) <ARRAY>
 * 2: Distance from the destination position the intermediate position
 *    should be (default: 250) <NUMBER>
 * 3: Maximum angular displacement for the intermediate position (default: 30) <NUMBER>
 *
 * Return:
 * Intermediate position, with same Z value as the destination position <ARRAY>
 *
 * Example:
 * [markerPos "spawnPos", markerPos "waypointPos", 300, 80] call rc_mf_fnc_generateIntermediatePosition
 *
 * Caller: Client / Server
 */

params ["_source", "_dest", ["_distance", 250], ["_maxAngle", 30]];

private _angleToSource = _dest getDir _source;
private _intermediateAngle = _angleToSource + (random _maxAngle) + (random _maxAngle) - _maxAngle;
_dest vectorAdd ([[_distance, 0], -_intermediateAngle] call BIS_fnc_rotateVector2D)

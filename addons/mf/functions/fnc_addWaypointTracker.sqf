#include "..\script_component.hpp"
/*
 * Adds a waypoint tracker to map controls that displays the distance and
 * direction to the players currently set custom waypoint (Shift + Click)
 * when rc_mf_fnc_shouldTrackWaypoint is true (default: in air vehicles).
 *
 * This function should automatically be called for the main map and GPS map
 * controls when the mission framework is loaded.
 *
 * Arguments:
 * 0: Map control <CONTROL>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [findDisplay 12 displayCtrl 51] call rc_mf_fnc_addWaypointTracker
 *
 * Caller: Client
 */

params ["_map"];

private _ehID = _map getVariable ["waypointEH", -1];
if (_ehID > -1) exitWith {};

_ehID = _map ctrlAddEventHandler ["Draw", {
  params ["_map"];
  if ([] call FUNC(shouldTrackWaypoint)) then {
    private _wppos = customWaypointPosition;
    if (count _wppos > 0) then {
      private _vehpos = getPosASL (vehicle player);
      _map drawLine [_vehpos, _wppos, [0,0,0,0.8]];
      _map drawIcon [
        "a3\ui_f\data\igui\cfg\actions\clear_empty_ca.paa",
        [0, 0, 0, 0.8],
        _vehpos, 32, 32,
        0,
        [_vehpos distance2D _wppos] call FUNC(humanDistance)
      ];
    };
  };
}];
_map setVariable ["waypointEH", _ehID];

#include "..\script_component.hpp"
/*
 * Called to determine whether the Deploy action, as well as other "home"
 * actions, should be available.
 *
 * By default, true when the player is in the homeArea, or when the mission has
 * not been started.
 *
 * Return:
 * Whether the player is allowed to deploy <BOOLEAN>
 *
 * Example:
 * [] call rc_mf_fnc_canDeploy
 *
 * Caller: Client
 */

private _isHome = [player] call FUNC(isHome);

// Is this a JIP deploy to the player's leader?
if (_isHome && {
  !([leader group player] call FUNC(isHome))
}) exitWith { true };

private _isLeader = (player == leader group player);

if (missionNamespace getVariable [QGVAR(missionStarted), false]) exitWith {
  // Is this a JIP deploy by a leader of a fresh group?
  _isHome && didJIP && _isLeader
};

// Non-JIP Deploys
if (deployAction == "none") exitWith { false };

if (deployAction == "once") exitWith { player == GVAR(topLeader) };

_isLeader

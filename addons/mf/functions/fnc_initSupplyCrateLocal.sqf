#include "..\script_component.hpp"
/*
 * Initializes the inventory and ace_cargo variables, on each client, of a
 * supply crate
 *
 * Arguments:
 * 0: Supply crate to initialize <OBJECT>
 * 1: Crate's name <STRING>
 * 2: Crate's inventory, in BIS_fnc_initAmmoBox format <ARRAY>
 *
 * Return:
 * Supply crate
 *
 * Caller: Client / Server
 */

params ["_crate", "_crateName", "_crateCargo"];

[_crate, [_crateCargo, false]] call BIS_fnc_initAmmoBox;

_crate setVariable ["ace_cargo_displayName", _crateName];
_crate setVariable ["ace_cargo_customName", _crateName];

_crate

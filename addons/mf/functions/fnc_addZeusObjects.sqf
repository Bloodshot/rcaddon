#include "..\script_component.hpp"
/*
 * Add objects to be editable by the framework's curator object.
 *
 * Arguments:
 * 0: Objects <ARRAY>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [_unit, _tank, _building] call rc_mf_fnc_addZeusObjects
 *
 * Caller: Server
 */

params ["_objects"];

if (isNil "zeus") exitWith {};

GVAR(zeus) addCuratorEditableObjects [_objects, true];

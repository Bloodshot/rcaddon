#include "..\script_component.hpp"
/*
 * Return the number of empty passenger seats in the given vehicle
 * (not driver / gunner / commander).
 *
 * Arguments:
 * 0: Vehicle <VEHICLE>
 *
 * Return:
 * [Number of empty cargo seats, number of empty turret seats] <ARRAY>
 *
 * Example:
 * ([helicopter] call rc_mf_fnc_emptySeats) params ["_emptyCargo", "_emptyTurrets"]
 *
 * Caller: Client / Server
 */

// Number of empty cargo seats (not driver / gunner / commander)
params ["_veh"];

private _cargo = [];
private _turrets = [];

{
  if (!(isNull (_x select 0)) || {
    (_x select 3) isEqualTo [0]
  }) then { continue };

  if (_x select 1 == "driver") then { continue };

  if (_x select 1 == "cargo") then {
    _cargo pushBack (_x select 2);
  } else {
    _turrets pushBack (_x select 3);
  };
} forEach (fullCrew [_veh, "", true]);

[_cargo, _turrets];

#include "..\script_component.hpp"
/*
 * Add actions to a vehicle that allow players to load resupply crates into its
 * cargo. Actions are available while the target isHome.
 *
 * Arguments:
 * 0: Vehicle or object <VEHICLE>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [veh] call rc_mf_fnc_addCrateActionsLocal
 *
 * Caller: Client
 */
params ["_target"];

{
  _ammoCrate = _x;
  _target addAction [
    format ["Load %1", _ammoCrate select 0],
    {
      _veh = _this select 0;
      _crate = [_this select 3, [0, 0, 0]] call FUNC(spawnSupplyCrate);
      _success = [_crate, _veh, true] call ace_cargo_fnc_loadItem;
      if (!_success) then { deleteVehicle _crate; };
    },
    _forEachIndex,
    (count GVAR(crates)) - _forEachIndex + 5,
    false,
    true,
    "",
    '[_this, _target] call FUNC(canAddCrate)',
    7
  ];
} forEach GVAR(crates);

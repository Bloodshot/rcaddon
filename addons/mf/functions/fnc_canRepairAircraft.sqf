#include "..\script_component.hpp"
/*
 * Whether the player should be allowed to instantaneously repair and rearm an
 * aircraft.
 *
 * By default, true when the aircraft is landed, off, and in the homeArea.
 *
 * Arguments:
 * 0: Aircraft <OBJECT>
 *
 * Return:
 * Whether to display the repair action <BOOLEAN>
 *
 * Example:
 * [helo1] call rc_mf_fnc_canRepairAircraft
 *
 * Caller: Client
 */

params ["_aircraft"];
[_aircraft] call FUNC(isHome) && {
  (getPos _aircraft) select 2 < 1
} && {
  vehicle player == player
} && {
  speed _aircraft < 1
} && {
  damage _aircraft < 1
} && {
  !isEngineOn _aircraft
};

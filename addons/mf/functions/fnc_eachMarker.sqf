#include "..\script_component.hpp"
/*
 * Iterate over each marker defined in an 3DEN layer, calling the provided code
 * with the marker's position, and optionally deleting the marker afterwards.
 *
 * Allows the mission maker to mark a set of positions in the editor (usually
 * with empty markers), and then do something to each of them in code.
 *
 * For the provided code, _this will be the marker's position.
 *
 * Arguments:
 * 0: 3DEN Layer Name containing the markers <STRING>
 * 1: Code to call for each marker position <CODE>
 * 2: Whether to delete markers afterwards (default: true) <BOOLEAN>
 *
 * Return:
 * Whether the layer was found <BOOLEAN>
 *
 * Example:
 * ["Supplies", {
 *   ["FIRETEAM", _this] call rc_mf_fnc_spawnSupplyCrate
 * }] call rc_mf_fnc_eachMarker
 *
 * Caller: Client / Server
 */

params ["_layer", "_code", ["_delete", true]];

private _entities = getMissionLayerEntities _layer;
if (count _entities < 2) exitWith {false};

{
  (markerPos _x) call _code;
  if (_delete) then {
    if (isServer) then {
      deleteMarker _x;
    } else {
      deleteMarkerLocal _x;
    };
  };
} forEach (_entities select 1);

true

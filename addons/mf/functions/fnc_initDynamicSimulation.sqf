#include "..\script_component.hpp"
/*
 * Ensure existing groups have dynamic simulation applied
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_initDynamicSimulation
 *
 * Caller: Server
 */

enableDynamicSimulationSystem true;

private _enemyGroups = allGroups select { !((side _x) in (missionNamespace getVariable ["friendlySides", [west]])) };
[_enemyGroups] remoteExec [QFUNC(initDynamicSimulationLocal), 0];

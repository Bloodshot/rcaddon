#include "..\script_component.hpp"
/*
 * Determines whether a position is underneath a roof
 *
 * Arguments:
 * 0: Position to test in Position ASL <ARRAY>
 * 1: Object to exclude from testing <OBJECT>
 *
 * Return:
 * 0: Is under roof <BOOLEAN>
 * 1: Building object owning the roof <OBJECT>
 *
 * Example:
 * [getPosASL player, player] call rc_mf_fnc_underRoof
 *
 * Caller: Client / Server
 */

params ["_pos", ["_ignore", objNull]];

private _posIgnore = objNull;

if (typeName _pos == "OBJECT") then {
  _posIgnore = _pos;
  _pos = getPosASL _pos;
};

private _intersections = lineIntersectsSurfaces [
  _pos,
  _pos vectorAdd [0, 0, 50],
  _posIgnore, _ignore, true, 1, "GEOM", "NONE"
] apply { _x select 3 };

private _index = _intersections findIf { _x isKindOf "Building" };

if (_index == -1) exitWith { [false, objNull] };

[true, _intersections select _index]

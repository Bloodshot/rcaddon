#include "..\script_component.hpp"
/*
 * Add actions to a vehicle that allow players to load resupply crates into its
 * cargo. Actions are available while the target isHome.
 *
 * Arguments:
 * 0: Vehicle or object <VEHICLE>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [veh] call rc_mf_fnc_addCrateActionsLocal
 *
 * Caller: Server
 */
params ["_target"];

[_target] remoteExec [QFUNC(addCrateActionsLocal), 0, true];

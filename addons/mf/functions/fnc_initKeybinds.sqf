#include "..\script_component.hpp"
/*
 * Initialize mission-specific keybinds
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_initKeybinds
 *
 * Caller: Client
 */

["Rainbow Coalition", QGVAR(collectiveLower), "Lower Collective", {
  [-0.07] call FUNC(adjustCollective);
}, {}, [0xF9, [false, true, false]]] call CBA_fnc_addKeybind;

["Rainbow Coalition", QGVAR(collectiveRaise), "Raise Collective", {
  [0.07] call FUNC(adjustCollective);
}, {}, [0xF8, [false, true, false]]] call CBA_fnc_addKeybind;

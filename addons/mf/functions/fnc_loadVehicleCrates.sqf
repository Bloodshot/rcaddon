#include "..\script_component.hpp"
/*
 * Spawn and load supply crates of the given types into a vehicle.
 *
 * Arguments:
 * 0: Vehicle to load crates into <VEHICLE>
 * 1: List of crate types to spawn and load <ARRAY>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [jeep, ["FIRETEAM", "RPG", "RPG"]] call rc_mf_fnc_loadVehicleCrates
 *
 * Caller: Client / Server
 */

[
  {
    params ["_vehicle", "_crateTypes"];
    typeName (_vehicle getVariable ["ace_cargo_space", objNull]) == "SCALAR"
  },
  {
    params ["_vehicle", "_crateTypes"];

    if (typeName _crateTypes == "STRING") then {
      _crateTypes = _this select 1;
    };

    {
      private _crate = [_x, [0, 0, 0]] call FUNC(spawnSupplyCrate);
      if (isNull _crate) then { continue };
      private _success = [_crate, _vehicle, true] call ace_cargo_fnc_loadItem;

      if (!_success) then { deleteVehicle _crate };
    } forEach _crateTypes;
  },
  _this,
  3,
  { "Failed to load crate into vehicle" call CBA_fnc_notify }
] call CBA_fnc_waitUntilAndExecute;

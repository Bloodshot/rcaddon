#include "..\script_component.hpp"
/*
 * Setup a player's radios based on their group's radio channel.
 * (group player) getVariable "radioChannel" must be defined.
 *
 * Sets the player's first push-to-talk based on the squadRadio config variable,
 * and second push-to-talk absed on the platoonRadio config variable.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_setACREChannels
 *
 * Caller: Client
 */

if (!isNil "platoonRadio") then {
  [platoonRadio, "default", 1, "Platoon Net", "PLTNET 1"] call acre_api_fnc_setPresetChannelField;
  [platoonRadio, "default"] call acre_api_fnc_setPreset;
};

waitUntil { [] call acre_api_fnc_isInitialized; };
waitUntil { sleep 2; !isNil {(group player) getVariable "radioChannel"} };

private _channel = (group player) getVariable "radioChannel";
private ["_squadRadio", "_platoonRadio"];

if (!isNil "squadRadio") then {
  _squadRadio = [squadRadio] call acre_api_fnc_getRadioByType;
};

if (!isNil "platoonRadio") then {
  _platoonRadio = [platoonRadio] call acre_api_fnc_getRadioByType;
};

private _radios = [];

if (!isNil "_squadRadio") then {
  [_squadRadio, _channel] call acre_api_fnc_setRadioChannel;
  [_squadRadio] call acre_api_fnc_setCurrentRadio;
  _radios pushBack _squadRadio;
};

if (!isNil "_platoonRadio") then {
  _radios pushBack _platoonRadio;
};

[_radios] call acre_api_fnc_setMultiPushToTalkAssignment;

#include "..\script_component.hpp"
/*
 * Adds an Open Arsenal action to the player that appears before deployment or
 * mission start.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_addArsenalAction
 *
 * Caller: Client
 */

private _actionID = player addAction [
  "Open Arsenal",
  { [] call FUNC(openArsenal) },
  [],
  5,
  false,
  true,
  "",
  '[] call FUNC(canOpenArsenal)'
];

player setVariable ["arsenalActionID", _actionID];

#include "..\script_component.hpp"
/*
 * Fill the transport slots of a vehicle with units of the given side.
 *
 * Arguments:
 * 0: Vehicle to fill <VEHICLE>
 * 1: Whether to only fill passenger seats (default: false) <BOOLEAN>
 * 2: From what side to create units (default: enemySide, or east) <SIDE>
 * 3: Max number of units to spawn, -1 to fill all seats (default: -1) <NUMBER>
 *
 * Return:
 * Spawned group <GROUP>
 *
 * Example:
 * [apc, true, west, 8] call rc_mf_fnc_fillTransportSlots
 *
 * Caller: Server
 */

params ["_vic", ["_cargoOnly", false], ["_side", missionNamespace getVariable ["enemySide", east]], ["_max", -1]];

private _count = { isNull (_x select 0) } count (fullCrew [_vic, ["", "cargo"] select _cargoOnly, true]);
if (_max > 0) then { _count = _count min _max };

private _group = [[0, 0, 0], _side, _count] call FUNC(spawnGroup);

if (_cargoOnly) then {
  { _x moveInCargo _vic } forEach (units _group);
} else {
  { _x moveInAny _vic } forEach (units _group);
};

_group addVehicle _vic;

_group

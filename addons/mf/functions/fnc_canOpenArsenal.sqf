#include "..\script_component.hpp"
/*
 * Called to determine whether the Arsenal action, should be available.
 *
 * By default, true when the player is in the homeArea, or when the mission has
 * not been started.
 *
 * Return:
 * Whether the player is allowed to open the arsenal <BOOLEAN>
 *
 * Example:
 * [] call rc_mf_fnc_canOpenArsenal
 *
 * Caller: Client
 */

[player] call FUNC(isHome) || {
  !(missionNamespace getVariable [QGVAR(missionStarted), false])
};

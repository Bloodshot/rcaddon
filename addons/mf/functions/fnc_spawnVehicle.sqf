#include "..\script_component.hpp"
/*
 * Spawn a vehicle of the given class and crew it with units drawn from the
 * specified side.
 *
 * If no spawn position is provided, one will be randomly
 * selected from the Vehicle Spawn Positions placed in editor, if any exist.
 *
 * If a target position is provided, the vehicle will be assigned a waypoint.
 * Troop trucks will disembark, and others will search and destroy.
 *
 * Arguments:
 * 0: Spawn position of vehicle, in Position ATL <ARRAY>
 * 1: Waypoint position, in Position ATL <ARRAY>
 * 2: Vehicle class <STRING>
 * 3: Whether the vehicle should be treated as a troop transport <BOOLEAN>
 * 4: Side of crew and troops (default: enemySide, or east) <SIDE>
 *
 * Return:
 * Vehicle's group <GROUP>
 *
 * Example:
 * [[], markerPos "attackMarker", "I_Truck_02_Covered_F", true, independent] call rc_mf_fnc_spawnVehicle
 *
 * Caller: Server
 */

params ["_spawnPos", "_targetPos", "_type", ["_troopTruck", false], ["_side", missionNamespace getVariable ["enemySide", east]]];

if (isNil "_spawnPos" || { count _spawnPos == 0 }) then {
  if (isNil QGVAR(vehicleSpawnPositions)) exitWith {};

  private _positions = GVAR(vehicleSpawnPositions) select {
    (_x distance _targetPos) > 1500 && {_x distance _targetPos < 3000};
  };

  if (count _positions == 0) then {
    _spawnPos = selectRandom GVAR(vehicleSpawnPositions);
  } else {
    _spawnPos = selectRandom _positions;
  };
};

if (isNil "_spawnPos") exitWith {
  ["GVAR(spawnVehicle) spawnPos is nil, no vehicle spawn positions"] call BIS_fnc_error;
};

private _air = _type isKindOf "Air";
private _special = ["NONE", "FLY"] select _air;
private _vic = createVehicle [_type, _spawnPos, [], 0, _special];
private _group = nil;
private _transportGroup = nil;

_vic setUnloadInCombat [false, false];
if (_troopTruck) then {
  if (_air) then {
    _group = { [_vic, _side] call FUNC(spawnCrew) } call CBA_fnc_directCall;
    _transportGroup = [_vic, true, _side, [12, 20] call BIS_fnc_randomInt] call FUNC(fillTransportSlots);
  } else {
    _group = [_vic, false, _side, [8, 14] call BIS_fnc_randomInt] call FUNC(fillTransportSlots);
    [_vic] spawn FUNC(watchTroopTruck);
  };
} else {
  _group = { [_vic, _side] call FUNC(spawnCrew) } call CBA_fnc_directCall;
};

if (isNil "_group") exitWith {
  deleteVehicle _vic;
  ["Could not spawn crew"] call BIS_fnc_error;
  nil
};

if (!isNil "_targetPos" && { count _targetPos > 0 }) then {
  private _wp = _group addWaypoint [_targetPos, 25];
  if (_troopTruck) then {
    if (isNil "_transportGroup") then {
      _wp setWaypointType "GETOUT";
      _wp = _group addWaypoint [_targetPos, 0];
      _wp setWaypointType "SAD";
    } else {
      _wp setWaypointType "TR UNLOAD";
      _wp = _transportGroup addWaypoint [_targetPos, 0];
      _wp setWaypointType "SAD";
      _group addWaypoint [_spawnPos, 0];
    };
  } else {
    if (_air) then {
      _wp setWaypointType "SAD";
    };
  };
};

if (!_troopTruck) then {
  [_vic] call FUNC(addDispersionEH);
};

[[_vic]] call FUNC(addZeusObjects);

_group

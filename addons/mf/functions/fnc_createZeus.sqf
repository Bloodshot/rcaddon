#include "..\script_component.hpp"
/*
 * Crate a curator object and ensure it is assigned to the admin.
 *
 * Sends a rc_mf_zeusCreated CBA event with the created zeus module.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_createZeus
 *
 * Caller: Server
 */

private _zeus = (createGroup sideLogic) createUnit ["ModuleCurator_F", [0, 0, 0], [], 0, "NONE"];

_zeus setVariable ["Owner", "#adminLogged", true];
_zeus setVariable ["Addons", 3, true];

// Call init manually
private _initFunction = getText ((configOf _zeus) >> "function");
[_zeus, [], true] call (missionNamespace getVariable _initFunction);

_zeus addCuratorEditableObjects [entities [[], []], true];
_zeus addCuratorEditableObjects [allMissionObjects "", true];
GVAR(zeus) = _zeus;
publicVariable QGVAR(zeus);

[QGVAR(zeusCreated), [GVAR(zeus)]] call CBA_fnc_globalEventJIP;

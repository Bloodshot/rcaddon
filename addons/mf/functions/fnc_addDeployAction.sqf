#include "..\script_component.hpp"
/*
 * Adds the Deploy action to the player available before the start of the
 * mission. Allows the player to deploy their group (or potentially everyone,
 * depending on configuration) using the configured deployMethod.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_addDeployAction
 *
 * Caller: Client
 */

private _actionID = player addAction [
  "<t color='#ee6666'>Deploy</t>",
  { [] call FUNC(doDeploy) },
  [],
  2,
  false,
  true,
  "",
  '[] call FUNC(canDeploy)'
];

player setVariable ["deployActionID", _actionID];

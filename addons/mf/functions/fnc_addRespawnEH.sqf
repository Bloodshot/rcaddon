#include "..\script_component.hpp"
/*
 * Handle player respawns by restoring their initial loadout, respawning them
 * at their initial position (rather than their current position), and adding a
 * JIP Deploy action.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_addRespawnEH
 *
 * Caller: Client
 */

player addEventHandler ["Respawn", {
  params ["_unit", "_corpse"];
  setPlayerRespawnTime 1E12;

  player setPosASL (_corpse getVariable QGVAR(initialPos));
  player setUnitLoadout (_corpse getVariable QGVAR(initialLoadout));
  player setVariable [QGVAR(initialLoadout), _corpse getVariable QGVAR(initialLoadout), true];
  player setVariable [QGVAR(initialPos), _corpse getVariable QGVAR(initialPos), true];

  [player, 0, true] call BIS_fnc_respawnTickets;

  if ([] call FUNC(canDeploy)) then {
    [] call FUNC(addJIPDeployAction);
  };
}];

player addEventHandler ["Killed", { setPlayerRespawnTime 1E12; }];

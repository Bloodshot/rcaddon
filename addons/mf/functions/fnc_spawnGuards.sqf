#include "..\script_component.hpp"
/*
 * Spawn a group of units, then order them to camp and defend their spawn position.
 *
 * Arguments:
 * 0: Spawn position in Position ATL <ARRAY>
 * 1: Group's side <SIDE>
 * 2: Number of units to spawn <NUMBER>
 *
 * Return:
 * Spawned group
 *
 * Example:
 * [markerPos "base", east, 6] call rc_mf_fnc_spawnGuards
 *
 * Caller: Server
 */

params ["_pos", "_side", "_count"];

private _defendGroup = [_pos, _side, _count] call FUNC(spawnGroup);
_defendGroup setVariable ["lambs_danger_enableGroupReinforce", false, true];
_defendGroup setVariable ["lambs_danger_dangerRadio", false, true];
_defendGroup setVariable ["lambs_danger_disableGroupAI", true, true];

private _wp = _defendGroup addWaypoint [_pos, 10];
_wp setWaypointType "GUARD";

{
  if ((random 1) > 0.4) then {
    doStop _x;
    _x spawn { sleep 0.5; _this action ["SitDown", _this]; };
  };
  _x setVariable ["lambs_danger_disableai", true, true];
} forEach (units _defendGroup);

_defendGroup

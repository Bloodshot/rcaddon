#include "..\script_component.hpp"
/*
 * Called every frame to update the rc_mf_snowEffect.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_snowPFH
 *
 * Caller: Client
 */

GVAR(snowEffect) setPosATL (positionCameraToWorld [0, 0, 0]);

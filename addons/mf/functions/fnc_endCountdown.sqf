#include "..\script_component.hpp"
/*
 * Instantly end an on-screen countdown triggered by rc_mf_fnc_displayCountdown
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_endCountdown
 *
 * Caller: Client
 */

RscFiringDrillTime_done = true;

#include "..\script_component.hpp"
/*
 * Create a snowy particle effect that follows the player, to give the illusion
 * of snowy weather.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_startSnow
 *
 * Caller: Client
 */

GVAR(snowEffect) = "#particlesource" createVehicleLocal (positionCameraToWorld [0, 0, 0]);
GVAR(snowEffect) setParticleParams [
  ["A3\Data_F\ParticleEffects\Universal\Universal", 16, 12, 8, 1],
  "", "Billboard",
  1,
  3,
  [0,0,5.5],
  [0,0,-1],
  1,
  0.01,
  0,
  0.01,
  [0.05,0.05],
  [[1,1,1,1]],
  [0, 1],
  0.2,
  1.2,
  "",
  "",
  GVAR(snowEffect),
  0,
  false
];

if (isNil "snowIntensity") then { GVAR(snowIntensity) = 600 };

GVAR(snowEffect) setParticleRandom [1.5, [15, 15, 3], [0, 0, 0], 0, 0.035, [0, 0, 0, 0.4], 0, 1, 0 , 1];
GVAR(snowEffect) setParticleCircle [25, [0, 0, 0]];
GVAR(snowEffect) setDropInterval (1 / GVAR(snowIntensity));

[FUNC(snowPFH), 0] call CBA_fnc_addPerFrameHandler;
[FUNC(snowRoofHandler), 1] call CBA_fnc_addPerFrameHandler;

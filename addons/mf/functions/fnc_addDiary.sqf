#include "..\script_component.hpp"
/*
 * Populates the diary with a list of entries under a given subject. The subject
 * will be created if it does not already exist. Each entry is a pair
 * [path, name], and entries should be ordered top-to-bottom.
 *
 * Each entry will be loaded from the given path, or skipped if it does not
 * exist. The contents of each entry's file should be in Structured Text format.
 *
 * Arguments:
 * 0: Subject Identifier <STRING>
 * 1: Subject Display Name, used when the subject has to be created <STRING>
 * 2: List of entries <ARRAY>
 *
 * Return:
 * Nothing
 *
 * Example:
 * ["Diary", "Briefing", [
 *   ["briefing\situation",  "Situation"],
 *   ["briefing\mission",    "Mission"],
 *   ["briefing\execution",  "Execution"]
 * ]] call rc_mf_fnc_addDiary;
 *
 * Caller: Client
 */

params ["_subject", "_subjectName", "_entries"];

reverse _entries;

if !(player diarySubjectExists _subject) then {
  player createDiarySubject [_subject, _subjectName];
};

{
  _x params ["_path", "_name"];

  if (!fileExists _path) then { continue };
  private _briefingText = loadFile _path;

  if (_briefingText == "") then { continue };

  if (_subject == "Diary") then {
    player createDiaryRecord [_subject, [_name, _briefingText]];
  } else {
    _briefingText = '<font size="20">' + _name + '</font><br /><br />' + _briefingText;
    player createDiaryRecord [_subject, [_name, _briefingText], taskNull, "?", false];
  };

} forEach _entries;

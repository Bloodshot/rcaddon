#include "..\script_component.hpp"
/*
 * Run rc_mf_fnc_aiDeathInventory on units when they are killed
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_initAiDeathInventory
 *
 * Caller: Server
 */

["Man", "Killed", {
  _this call FUNC(aiDeathInventory);
}] call CBA_fnc_addClassEventHandler;

#include "..\script_component.hpp"
/*
 * Spawn a random enemy infantry group or vehicle based on the provided
 * armorFactor. A higher armorFactor allows heavier vehicle types to spawn, and
 * makes them more likely. A armorFactor above 1 decreases the chance for
 * infantry to spawn. At 2, only vehicles will spawn.
 *
 * Only the default vehicle types are candidates. Mission specific vehicles
 * types will not be spawned from this function.
 *
 * Arguments:
 * 0: Spawn position in Position ATL <ARRAY>
 * 1: Destination waypoint position in Position ATL <ARRAY>
 * 2: Group's side <SIDE>
 * 3: Armor factor, from 0 to 2 (default: armorFactor, or 1) <NUMBER>
 *
 * Return:
 * Spawned group <GROUP>
 *
 * Example:
 * [markerPos "vehiclePatrolStart", [], east, 2] call rc_mf_fnc_spawnRandomArmorGroup
 *
 * Caller: Server
 */

params ["_spawnPosition", "_dest", "_side", ["_armorFactor", missionNamespace getVariable ["armorFactor", 1]]];

private _weightInfantry   = linearConversion [0.80, 1.00, _armorFactor, 2,  1.0, true]
                          + linearConversion [1.00, 2.00, _armorFactor, 0, -1.0, true];
private _weightTroopTruck = linearConversion [0.00, 0.20, _armorFactor, 0,  1.5, true]
                          + linearConversion [0.40, 0.70, _armorFactor, 0, -1.0, true];
private _weightAV         = linearConversion [0.10, 0.30, _armorFactor, 0,  1.5, true]
                          + linearConversion [0.75, 0.90, _armorFactor, 0, -1.0, true];
private _weightAPC        = linearConversion [0.40, 0.65, _armorFactor, 0,  1.5, true];
private _weightIFV        = linearConversion [0.60, 0.80, _armorFactor, 0,  1.0, true];
private _weightTank       = linearConversion [0.70, 0.90, _armorFactor, 0,  0.8, true];

private _vehiclePool = GVAR(vehicleTemplates) select (_side call BIS_fnc_sideID);

if (!("truck" in _vehiclePool) || { count (_vehiclePool get "truck") == 0 }) then { _weightTroopTruck  = 0 };
if (!("av"    in _vehiclePool) || { count (_vehiclePool get "av")    == 0 }) then { _weightAV          = 0 };
if (!("apc"   in _vehiclePool) || { count (_vehiclePool get "apc")   == 0 }) then { _weightAPC         = 0 };
if (!("ifv"   in _vehiclePool) || { count (_vehiclePool get "ifv")   == 0 }) then { _weightIFV         = 0 };
if (!("tank"  in _vehiclePool) || { count (_vehiclePool get "tank")  == 0 }) then { _weightTank        = 0 };

private _type = selectRandomWeighted [
  "infantry", _weightInfantry,
  "truck",    _weightTroopTruck,
  "av",       _weightAV,
  "apc",      _weightAPC,
  "ifv",      _weightIFV,
  "tank",     _weightTank
];

if (_type == "infantry") then {
  [_spawnPosition, _dest, _side, round (random [6, 9, 12])] call FUNC(spawnEnemyInfantry);
} else {
  private _pos = [_spawnPosition, 100] call BIS_fnc_nearestRoad;
  if (isNull _pos) then {
    _pos = [_spawnPosition, 0, 40, 8, 0, 0.3] call BIS_fnc_findSafePos;
    if (count _pos == 3) then { _pos = nil };
  } else {
    _pos = getPosATL _pos;
  };
  if (isNil "_pos") exitWith { nil };

  [_pos, _dest, _type, _side] call FUNC(spawnVehicleFromPool);
};

#include "..\script_component.hpp"
/*
 * Update the BFT markers. Currently only supports tracking friendly pilots.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_updateBFTMarkers
 *
 * Caller: Client
 */

private _currentlyTracking = allPlayers select {[_x] call FUNC(shouldBFTTrack)};

{
  private _marker = _x getVariable QGVAR(bftMarker);
  if (isNil "_marker") then {
    _marker = createMarkerLocal [format [QGVAR(bftMarker)%1, owner _x], getPosASL _x];
    _marker setMarkerTypeLocal "b_air";
    _marker setMarkerColorLocal "ColorWest";
    _marker setMarkerTextLocal (groupId (group _x));
    _x setVariable [QGVAR(bftMarker), _marker];
  };

  _marker setMarkerPosLocal (getPosASL _x);
} forEach _currentlyTracking;

{
  private _marker = _x getVariable QGVAR(bftMarker);
  if !(isNil "_marker") then {
    deleteMarkerLocal _marker;
    _x setVariable [QGVAR(bftMarker), nil];
  };
} forEach (GVAR(bftTrackedPlayers) - _currentlyTracking);

GVAR(bftTrackedPlayers) = _currentlyTracking;

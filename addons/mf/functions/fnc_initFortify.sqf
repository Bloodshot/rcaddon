#include "..\script_component.hpp"
/*
 * Initialize the ACE Fortify system.
 *
 * Collects allowed fortifications from a template defined by the
 * "Fortifications" 3DEN layer. Each object in this layer may be
 * assigned a cost by setting the "fortifyCost" variable on it,
 * usually in its init.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_initFortify
 *
 * Caller: Server
 */
params [["_budget", 350]];

if (count (getMissionLayerEntities "Fortifications") > 0) then {
  GVAR(fortifications) = (getMissionLayerEntities "Fortifications" select 0) apply {
    private _ret = [typeOf _x, _x getVariable ["fortifyCost", 250]];
    deleteVehicle _x;

    _ret
  };

  GVAR(fortifications) = [GVAR(fortifications), [], { _x select 1 }, "ASCEND"] call BIS_fnc_sortBy;

  {
    [_x, _budget, GVAR(fortifications)] call ace_fortify_fnc_registerObjects;
  } forEach (missionNamespace getVariable ["friendlySides", [west]]);
};

missionNamespace setVariable ["ace_fortify_fortifyAllowed", true, true];
missionNamespace setVariable ["ace_fortify_timeCostCoefficient", 0, true];
missionNamespace setVariable ["ace_fortify_timeMin", 3, true];

[] remoteExec [QFUNC(initFortifyLocal), GVAR(allClients), true];

[QGVAR(missionStarted), {
  {
    _x removeItem "ACE_Fortify";
  } forEach allUnits;

  missionNamespace setVariable ["ace_fortify_fortifyAllowed", false, true];
}] call CBA_fnc_addEventHandler;

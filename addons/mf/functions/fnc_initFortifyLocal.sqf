#include "..\script_component.hpp"
/*
 * Initialize the ACE Fortify system on a client
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_initFortifyLocal
 *
 * Caller: Client
 */

 player addItem "ACE_Fortify";

#include "..\script_component.hpp"
/*
 * Spawn an extract helicopter that will land by the specified position and will
 * take off when a player uses the "Lift!" addAction.
 *
 * Arguments:
 * 0: Destination position in Position ATL <ARRAY>
 * 1: Helicopter class (default: friendlyHeloClass) <STRING>
 * 2: Spawn position in Position2D (default: markerPos "planeStart") <ARRAY>
 * 3: Pilot object (default: nil) <UNIT>
 * 4: Pilot's side (default: friendlySides select 0, or west) <SIDE>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [markerPos "extract"] call rc_mf_fnc_spawnExtract
 *
 * Caller: Server
 */

params ["_pos",
  ["_heloClass", missionNamespace getVariable "friendlyHeloClass"],
  ["_startPos", markerPos "planeStart"],
  ["_pilot", nil],
  ["_side", nil]
];

if (isNil "_heloClass") exitWith {
  ["friendlyHeloClass not defined"] call BIS_fnc_error;
};

if (isNil "_side") then {
  _side = (missionNamespace getVariable ["friendlySides", [west]]) select 0;
};

_startPos set [2, 200];

private _heli = createVehicle [_heloClass, _startPos, [], 0, "FLY"];
_heli setPosATL _startPos;

if (isNil "_pilot") then {
  private _tmpgrp = createGroup _side;
  _pilot = _tmpgrp createUnit ["C_Man_Pilot_F", [0, 0, 0], [], 0, "NONE"];
  [_pilot] joinSilent _tmpgrp;
  deleteGroup _tmpgrp;
};

_pilot moveInDriver _heli;
(group _pilot) setCombatBehaviour "CARELESS";

private _wp = (group _pilot) addWaypoint [_pos, 0];
_wp setWaypointType "SCRIPTED";
_wp setWaypointScript "A3\functions_f\waypoints\fn_wpLand.sqf";

[_heli, ["<t color='#ee6666'>Lift!</t>", {
  params ["_target", "_caller", "", "_arguments"];
  _arguments params ["_startPos"];

  _startPos set [2, 200];
  (group (driver _target)) addWaypoint [_startPos, -1];
}, [_startPos], 2, false, true, "", "true"]] remoteExec ["addAction", 0];

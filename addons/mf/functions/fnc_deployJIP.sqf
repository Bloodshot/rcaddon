#include "..\script_component.hpp"
/*
 * Deploy a unit to their leader, or deploy a leader's group from the homeArea
 * to the specified position. Usually triggered from the Deploy action of a
 * JIPed player.
 *
 * Arguments:
 * 0: Player that triggered the deploy <UNIT>
 * 1: Desired deploy position in Position ATL <ARRAY>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [player, markerPos "reinforceMarker"] remoteExec ["rc_mf_fnc_deployJIP", 2]
 *
 * Caller: Server
 */

params ["_caller", "_position"];

private _vehs = [vehicle _caller];

if (!alive (leader group _caller) || { _caller == (leader group _caller) }) then {
  private _units = units group _caller select {
    [_x] call FUNC(isHome);
  };

  {
    _vehs pushBackUnique (vehicle _x);
  } forEach _units;
};

{
  private _targetPosition = [_position, 0, 8, 2] call BIS_fnc_findSafePos;
  if (count _targetPosition == 3) then { _targetPosition = _position };
  _targetPosition set [2, 0];
  _x setPosATL _targetPosition;
  [] remoteExec [QFUNC(removeHomeActions), _x];
} forEach _vehs;

#include "..\script_component.hpp"
/*
 * Fix an AI to their position until an enemy closes to within close range.
 *
 * Arguments:
 * 0: Unit to hold in their position <UNIT>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [garrisonedUnit] call rc_mf_fnc_holdPosition
 *
 * Caller: Server
 */

params ["_unit"];

if (vehicle _unit != _unit) exitWith {};

doStop vehicle _unit;
_unit disableAI "PATH";

private _scriptHandle = _unit getVariable QGVAR(holdScriptHandle);
if (!isNil "_scriptHandle" && {!scriptDone _scriptHandle}) exitWith {};

_scriptHandle = [_unit] spawn {
  params ["_unit"];
  while { true } do {
    if (!alive _unit) exitWith {};

    // "Un-leash" the unit if it knows of any nearby hostiles
    if (
        {
          (_x select 3) > 0 // Hostile units
        } count (_unit nearTargets 30) > 0
    ) exitWith {
      _unit enableAI "PATH";
      _unit doFollow (leader _unit);
      _unit forceSpeed -1;
    };

    // Stagger wakeup times so as not to hammer the server all at once
    sleep (5 + random 1);
  };
};

_unit setVariable [QGVAR(holdScriptHandle), _scriptHandle];

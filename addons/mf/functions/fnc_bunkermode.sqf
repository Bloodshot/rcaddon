#include "..\script_component.hpp"
/*
 * Toggles Bunker Mode. When Bunker Mode is enabled, any group of buildings placed
 * by Zeus will automatically be garrisoned by a group of units, via a call to
 * rc_mf_fnc_garrisonNearby.
 *
 * Arguments:
 * 0: Number of units to spawn (default: 6) <NUMBER>
 * 1: Radius to search for placed buildings in (default: 20) <NUMBER>
 * 2: Side of units to spawn (default: enemySide or east) <SIDE>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [10, 40, west] call rc_mf_fnc_bunkermode
 *
 * Caller: Client
 */

private _zeus = getAssignedCuratorLogic player;

if (isNull _zeus) exitWith {};

if (isNil QGVAR(bunkermode_on)) then {
  GVAR(bunkermode_on) = true;
} else {
  GVAR(bunkermode_on) = !GVAR(bunkermode_on);
};

if (GVAR(bunkermode_on)) then {
  "Bunker Mode Enabled: Buildings will automatically be garrisoned" call CBA_fnc_notify;

  GVAR(bunkermode_params) = _this;

  GVAR(bunkermode_groupEh) = _zeus addEventHandler ["CuratorGroupPlaced", {
    params ["_curator", "_group"];
    GVAR(bunkermode_params) params [["_count", 6], ["_range", 20], ["_side", missionNamespace getVariable ["enemySide", east]]];

    if (isNull _group) then {
      private _pos = GVAR(bunkermode_lastPos);
      [_pos, _range, _side, _count, 4] remoteExec [QFUNC(garrisonNearby), 2];
    };
  }];

  GVAR(bunkermode_objectEH) = _zeus addEventHandler ["CuratorObjectPlaced", {
    params ["_curator", "_entity"];

    if (_entity isKindOf "Building") then {
      GVAR(bunkermode_lastPos) = getPosATL _entity;
    };
  }];
} else {
  "Bunker Mode Disabled" call CBA_fnc_notify;

  if (!isNil QGVAR(bunkermode_groupEH)) then {
    _zeus removeEventHandler ["CuratorGroupPlaced", GVAR(bunkermode_groupEH)];
    GVAR(bunkermode_groupEH) = nil;
  };

  if (!isNil QGVAR(bunkermode_objectEH)) then {
    _zeus removeEventHandler ["CuratorObjectPlaced", GVAR(bunkermode_objectEH)];
    GVAR(bunkermode_objectEH) = nil;
  };
};

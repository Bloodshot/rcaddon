#include "..\script_component.hpp"
/*
 * Instruct all editor placed AI units without waypoints to holdPosition (see
 * rc_mf_fnc_holdPosition)
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_haltEditorUnits
 *
 * Caller: Server
 */

{
  if (count waypoints _x == 0) then {
    {
      [_x] call holdPosition;
    } forEach (units _x);
  };
} forEach allGroups;

#include "..\script_component.hpp"
/*
 * Handle rc_mf_fnc_mapClick in the Zeus interface
 *
 * Caller: Client
 */

params ["_code", ["_args", []], ["_mode", "SINGLE"], ["_color", [1,0,0,1]], ["_markerColor", "ColorBLACK"],
                 ["_sourcePos", nil], ["_text", ""], ["_icon", "\a3\ui_f\data\IGUI\Cfg\Cursors\selectOver_ca.paa"], ["_angle", 0]];

GVAR(mapClick_posArray) = [];
GVAR(mapClick_markers) = [];

if (!isNil "_sourcePos") then {
  GVAR(mapClick_posArray) pushBack _sourcePos;
};

if (missionNamespace getVariable [QGVAR(mapClick_zeus_running), false]) exitWith {
  [false, _args, [0, 0, 0], [0, 0, 0], false, false, false] call _code;
};

GVAR(mapClick_zeus_running) = true;

// Add mouse button eh for the zeus display (triggered from 2d or 3d)
GVAR(mapClick_zeus_displayEHMouse) = [findDisplay 312, "mouseButtonDown", {
  params ["", "_mouseButton", "", "", "_shift", "_ctrl", "_alt"];

  if (_mouseButton != 0) exitWith {}; // Only watch for LMB

  _thisArgs params ["_mode", "_markerColor", "_code", "_codeArgs"];

  // Get mouse position on 2D map or 3D world
  private _mousePosAGL = if (ctrlShown ((findDisplay 312) displayCtrl 50)) then {
    private _pos2d = (((findDisplay 312) displayCtrl 50) ctrlMapScreenToWorld getMousePosition);
    _pos2d set [2, getTerrainHeightASL _pos2d];
    ASLtoAGL _pos2d
  } else {
    private _clickedIDC = ctrlIDC ((findDisplay 312) ctrlAt getMousePosition);
    // Exit if clicked on the UI
    if (_clickedIDC != 53) exitWith {};

    [] call FUNC(mapClick_intersection);
  };

  if (!isNil "_mousePosAGL") then {
    GVAR(mapClick_zeus_running) = !([_mousePosAGL, [_mode, _markerColor, _code, _codeArgs], _shift, _ctrl, _alt] call FUNC(mapClick_onClick));
  };
}, [_mode, _markerColor, _code, _args]] call CBA_fnc_addBISEventHandler;

// Add key eh for the zeus display (triggered from 2d or 3d)
GVAR(mapClick_zeus_displayEHKeyboard) = [findDisplay 312, "KeyDown", {
  params ["", "_keyCode", "_shift", "_ctrl", "_alt"];

  if (_keyCode != 1) exitWith {}; // Only watch for ESC

  GVAR(mapClick_zeus_running) = false;
  true
}, [_args, _code]] call CBA_fnc_addBISEventHandler;

// Add draw EH for the zeus map - draws the 2D icon and line
GVAR(mapClick_zeus_mapDrawEH) = [((findDisplay 312) displayCtrl 50), "draw", {
  params ["_mapCtrl"];
  _thisArgs params ["_text", "_icon", "_color", "_angle"];

  private _pos2d = (((findDisplay 312) displayCtrl 50) ctrlMapScreenToWorld getMousePosition);
  _mapCtrl drawIcon [_icon, _color, _pos2d, 24, 24, _angle, _text, 1, 0.03, "TahomaB", "right"];

  if (count GVAR(mapClick_posArray) > 0) then {
    _mapCtrl drawLine [GVAR(mapClick_posArray) select 0, _pos2d, _color];
  };
}, [_text, _icon, _color, _angle]] call CBA_fnc_addBISEventHandler;

// Add draw EH for 3D camera view - draws the 3D icon and line
[{
  (_this select 0) params ["_args", "_code", "_text", "_icon", "_color", "_angle"];
  if (( isNull findDisplay 312 ) || { !isNull findDisplay 49 }) then {
    GVAR(mapClick_zeus_running) = false;
  };

  if (GVAR(mapClick_zeus_running)) then {
    // Draw the 3d icon and line
    private _mousePosAGL = screenToWorld getMousePosition;
    drawIcon3D [_icon, _color, _mousePosAGL, 1.5, 1.5, _angle, _text];
    if (count GVAR(mapClick_posArray) > 0) then {
      drawLine3D [(GVAR(mapClick_posArray) select 0) vectorAdd [0, 0, 1.8], _mousePosAGL vectorAdd [0, 0, 1.8], _color];
      drawLine3D [_mousePosAGL, _mousePosAGL vectorAdd [0, 0, 1.8], _color];
    };
  } else {
    (_this select 1) call CBA_fnc_removePerFrameHandler;
    (findDisplay 312) displayRemoveEventHandler ["mouseButtonDown", GVAR(mapClick_zeus_displayEHMouse)];
    (findDisplay 312) displayRemoveEventHandler ["KeyDown", GVAR(mapClick_zeus_displayEHKeyboard)];
    ((findDisplay 312) displayCtrl 50) ctrlRemoveEventHandler ["draw", GVAR(mapClick_zeus_mapDrawEH)];
    GVAR(mapClick_zeus_displayEHMouse) = nil;
    GVAR(mapClick_zeus_displayEHKeyboard) = nil;
    GVAR(mapClick_zeus_mapDrawEH) = nil;
    GVAR(mapClick_zeus_sourcePos) = nil;

    { deleteMarkerLocal _x } forEach GVAR(mapClick_markers);
    GVAR(mapClick_posArray) = nil;
    GVAR(mapClick_markers) = nil;
  };
}, 0, [_args, _code, _text, _icon, _color, _angle]] call CBA_fnc_addPerFrameHandler;

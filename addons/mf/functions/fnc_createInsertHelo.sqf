#include "..\script_component.hpp"
/*
 * Creates one helicopter for deployMethod = "helicopter" insertion, and directs
 * them to land at the provided destination. The helicopter will take off again,
 * and eventually despawn, once all of its passengers have disembarked.
 *
 * Called from rc_mf_fnc_deploy
 *
 * Arguments:
 * 0: Destination in Position ATL <ARRAY>
 * 1: Class of helicopter to spawn (default: friendlyHeloClass) <STRING>
 * 2: Spawn position in Position 2D (default: markerPos "planeStart") <ARRAY>
 * 3: Side of the pilot to spawn (default: friendlySides select 0, or civilian) <SIDE>
 *
 * Return:
 * [heli, pilot] <ARRAY>
 *
 * Example:
 * [markerPos "landingMarker", "B_Heli_Transport_03_unarmed_F", independent] call rc_mf_fnc_createInsertHelo
 *
 * Caller: Server
 */

params ["_dest", ["_heloClass", friendlyHeloClass], ["_pos", markerPos "planeStart"], ["_side", nil]];

if (isNil "_heloClass") exitWith {
  ["No friendlyHeloClass defined"] call BIS_fnc_error;
};

if (isNil "_side") then {
  if (count (missionNamespace getVariable ["friendlySides", []]) > 0) then {
    _side = friendlySides select 0;
  } else {
    _side = civilian;
  };
};

_pos = +_pos;
_pos set [2, 100];
_pos = _pos vectorAdd ([[100, 0, 0], random 360] call BIS_fnc_rotateVector2D);
_pos = _pos vectorAdd [0, 0, random 60];

private _heli = createVehicle [friendlyHeloClass, _pos, [], 0, "FLY"];
_heli setPosATL _pos;
_heli setDir (_heli getDir _dest);
_heli flyInHeight 100;
private _pilot = (createGroup _side) createUnit ["C_Man_Pilot_F", [0, 0, 0], [], 0, "NONE"];
_pilot moveInDriver _heli;
(group _pilot) setCombatBehaviour "CARELESS";

private _wp = (group _pilot) addWaypoint [_dest, -1];
_wp setWaypointType "SCRIPTED";
_wp setWaypointScript "A3\functions_f\waypoints\fn_wpLand.sqf";

[_heli, _pilot] spawn {
  params ["_heli", "_pilot"];
  waitUntil {
    sleep 1;
    (count crew _heli == 1)
  };

  sleep 4;

  private _initialPos = getPosASL _heli;

  private _pos = markerPos "planeStart";
  _pos set [2, 200];
  (group _pilot) addWaypoint [_pos, -1];

  waitUntil {
    sleep 1;
    (_heli distance2D _initialPos > 2000)
  };

  deleteVehicle _pilot;
  deleteVehicle _heli;
};

[_heli, _pilot];

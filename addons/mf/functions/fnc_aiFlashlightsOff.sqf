#include "..\script_component.hpp"
/*
 * Disable AI flashlights on all existing groups
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_aiFlashlightsOff
 *
 * Caller: Server
 */

{ _x enableGunLights "FORCEOFF" } forEach allGroups;

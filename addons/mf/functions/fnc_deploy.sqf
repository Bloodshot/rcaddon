#include "..\script_component.hpp"
/*
 * Deploys units, according to config. Usually triggered by the Deploy
 * addAction.
 *
 * This is a complex function that can do many different things depending on the
 * deploy* configuration variables.
 *
 * Deployment will signal the start of the mission (rc_mf_fnc_startMission) if
 * all players have deployed (are no longer in the homeArea).
 *
 * Arguments:
 * 0: Player that triggered the deploy <UNIT>
 * 1: Desired deploy position, for deployDestination = "default", in Position ATL <ARRAY>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [player, markerPos "deployMarker"] remoteExec ["rc_mf_fnc_deploy", 2]
 *
 * Caller: Server
 */

params ["_caller", "_position"];

private _groups = if (missionNamespace getVariable ["deployAction", "leaders"] == "once") then {
  allGroups select {
    (side _x) in (missionNamespace getVariable ["friendlySides", []]) && {
      // Has at least one player
      { isPlayer _x } count (units _x) > 0;
    };
  };
} else {
  [group _caller];
};

private ["_transport", "_seats", "_oldPosition", "_filled"]; // Written to by _createGroupTransports
private ["_cargoTransport", "_lastTransportDir", "_lastTransportPos"]; // Used by paradrop

// Creates a pool of vehicle transports that are sufficient
// to transport a a given group's units

// Groups will be kept together, if possible, but will be
// deployed using the previous group's vehicle if it is large enough

private _createGroupTransports = {
  params ["_units", "_createNewTransport"];

  // Continue to fill the already existing transport if we can fit the whole group
  // If we can't, make a new one
  private ["_cargoSeats", "_turretSeats"];
  if (isNil "_transport" || {
    (count _cargoSeats) + count _turretSeats < count _units
  } || {
    _oldPosition isNotEqualTo _groupPosition
  }) then {
    _transport = [] call _createNewTransport;
    _seats = [_transport] call FUNC(emptySeats);
    _filled = 0;
  };

  _cargoSeats = _seats select 0;
  _turretSeats = _seats select 1;

  if (count _cargoSeats + count _turretSeats == 0) exitWith {
    ["Cannot create transport: no empty passenger seats in vehicle class!"] call BIS_fnc_error;
  };

  {
    if (_filled >= count _cargoSeats + count _turretSeats) then {
      _transport = [] call _createNewTransport;
      _seats = [_transport] call FUNC(emptySeats);
      _cargoSeats = _seats select 0;
      _turretSeats = _seats select 1;
      _filled = 0;
    };

    if (_filled < count _turretSeats) then {
      [_x, [_transport, _turretSeats select _filled]] remoteExec ["moveInTurret", _x];
    } else {
      [_x, [_transport, _cargoSeats select (_filled - (count _turretSeats)), true]] remoteExec ["moveInCargo", _x];
    };
    _filled = _filled + 1;
  } forEach _units;
};

{
  private _group = _x;
  private _groupPosition = nil;
  private _deployDir = 0; // Used to orient boats

  if (deployDestination == "marker" && isNil "_position") then {
    private _marker = [_group] call FUNC(getDeployMarker);
    _groupPosition = markerPos _marker;
    _deployDir = markerDir _marker;
  } else {
    _groupPosition = _position;
  };

  private _units = (units _group);
  private _vehs = [];
  {
    _vehs pushBackUnique (vehicle _x);
  } forEach _units;
  private _infantry = _vehs select { _x isKindOf "Man" };
  private _vehicles = _vehs - _infantry;

  private _deployMethod = _group getVariable ["deployMethod", missionNamespace getVariable ["deployMethod", "default"]];

  switch (_deployMethod) do {
    case "boat": {
      private _i = 0;

      private _pos = _groupPosition;

      {
        if (getNumber (configOf _x >> "canFloat") == 1) then {
          if (!isNil "_lastTransportPos") then {
            _pos = _lastTransportPos vectorAdd ([[10, 0, 0], -_lastTransportDir] call BIS_fnc_rotateVector2D);
          };

          private _vehPos = [_pos, 0, 10, 10, 2] call BIS_fnc_findSafePos;
          _vehPos set [2, 0];
          _x setPosASLW _vehPos;
          _x setDir _deployDir;
        };

        _lastTransportPos = _pos;
        _lastTransportDir = getDir _x;
      } forEach _vehicles;

      while { _i < count _infantry } do {
        if (isNil "_transport" || {
          _transport emptyPositions "" < (count _infantry) - _i
        }) then {
          if (!isNil "_lastTransportPos") then {
            _pos = _lastTransportPos vectorAdd ([[10, 0, 0], -_lastTransportDir] call BIS_fnc_rotateVector2D);
          };

          private _boatPos = [_pos, 0, 10, 10, 2] call BIS_fnc_findSafePos;
          _transport = (missionNamespace getVariable ["friendlyBoatClass", "I_G_Boat_Transport_01_F"]) createVehicle _boatPos;
          _transport setDir _deployDir;

          _lastTransportPos = _boatPos;
          _lastTransportDir = getDir _transport;
        };

        while { (_infantry select _i) moveInAny _transport } do {
          _i = _i + 1;
          if (_i == count _infantry) then { break };
        };

        if (count (units _transport) == 0) exitWith {
          ["Could not deploy: could not load units into boat"] call BIS_fnc_error;
        };
      };
    };

    case "helicopter": {
      [_infantry, { ([_groupPosition] call FUNC(createInsertHelo)) select 0 }] call _createGroupTransports;
    };

    case "paradrop": {
      if (isNil "friendlyPlaneClass") exitWith {
        ["friendlyPlaneClass not defined"] call BIS_fnc_error;
      };

      private _newPlane = {
        private _pos = markerPos "planeStart";
        private _offset = [0, 0, 0];
        if !(isNil "_lastTransportPos") then {
          _offset = ([[random 400 - 200, 300, 60], 180 - _lastTransportDir] call BIS_fnc_rotateVector2D);
          _pos = _lastTransportPos vectorAdd _offset;
        };

        private _transport = ([_groupPosition vectorAdd _offset, friendlyPlaneClass, _pos] call FUNC(createPlane)) select 0;

        [_groupPosition, _transport, driver _transport] spawn FUNC(paradropTracker);

        _lastTransportPos = getPosATL _transport;
        _lastTransportDir = getDir _transport;

        _transport
      };

      [_infantry, _newPlane] call _createGroupTransports;

      // Unable to be used vehicle-in-vehicle in the given plane class
      private _noVIV = [];

      {
        if (isNil "_cargoTransport" || {
          (_cargoTransport canVehicleCargo _x) params ["_can", "_canEmpty"];
          _canEmpty && !_can
        }) then {
          _cargoTransport = [] call _newPlane;
          _cargoTransport enableVehicleCargo true;
        };

        if ((_cargoTransport canVehicleCargo _x) select 1) then {
          _cargoTransport setVehicleCargo _x;
        } else {
          _noVIV pushBack _x;
        };
      } forEach _vehicles;

      if (count _noVIV > 0) then {
        if (isNil "_cargoTransport") then {
          _cargoTransport = [] call _newPlane;
          _cargoTransport enableVehicleCargo true;
        };

        private _dropVehs = _cargoTransport getVariable [QGVAR(doParadrop_dropVehicles), []];
        _dropVehs append _noVIV;
        _cargoTransport setVariable [QGVAR(doParadrop_dropVehicles), _dropVehs];
      };
    };

    default {
      {
        if (_x isKindOf "Air") then { continue };
        private _targetPosition = [_groupPosition, 0, 15, 5] call BIS_fnc_findSafePos;
        if (count _targetPosition == 3) then { _targetPosition = _position };
        _targetPosition set [2, 0];
        _x setPosATL _targetPosition;
        if !(_x isKindOf "Man") then {
          _x setDir _deployDir;
        };
      } forEach _vehs;
    };
  };

  _oldPosition = _groupPosition;
} forEach _groups;

if !(missionNamespace getVariable ["firstDeploy", false]) then {
  GVAR(firstDeploy) = true;
  if (missionNamespace getVariable ["startMissionOn", "deploy"] == "deploy") then {
    [
      {
        allPlayers findIf { [_x] call FUNC(isHome) } == -1
      },
      FUNC(startMission)
    ] call CBA_fnc_waitUntilAndExecute;
  };
};

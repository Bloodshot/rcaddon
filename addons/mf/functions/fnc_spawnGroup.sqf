#include "..\script_component.hpp"
/*
 * Spawn a group of units from the specified side's template
 *
 * Arguments:
 * 0: Spawn position in Position ATL <ARRAY>
 * 1: Group's side <SIDE>
 * 2: Number of units to spawn <NUMBER>
 *
 * Return:
 * Spawned group <GROUP>
 *
 * Example:
 * [markerPos "spawnPos", independent, 12] call rc_mf_fnc_spawnGroup
 *
 * Caller: Server
 */

params ["_pos", "_side", "_count"];

private _groupTemplate = selectRandom (GVAR(groupTemplates) select (_side call BIS_fnc_sideID));
if (isNil "_groupTemplate") exitWith {};

private _tmpGroup = createGroup _side;

for "_i" from 1 to _count do {
  (selectRandom _groupTemplate) params ["_class", "_loadout"];
  private _unit = _tmpGroup createUnit [_class, _pos, [], 5, "NONE"];
  [_unit] call FUNC(addDispersionEH);

  if (count _loadout > 0) then {
    _unit setUnitLoadout _loadout;
  };
};

private _group = createGroup [_side, true];
(units _tmpGroup) joinSilent _group;

deleteGroup _tmpGroup;

private _launcherSpawnChance = missionNamespace getVariable ["launcherSpawnChance", 1];

{
  _x setSkill (missionNamespace getVariable ["aiSkill", 0.3]);
  if (random 1 > _launcherSpawnChance) then {
    private _launcher = secondaryWeapon _x;
    if (_launcher != "") then { _x removeWeapon _launcher };
  };
} forEach (units _group);
_group enableGunLights "FORCEOFF";

[units _group] call FUNC(addZeusObjects);

_group

#include "..\script_component.hpp"
/*
 * Spawn a group of units, then order them to patrol around their spawn position
 *
 * Arguments:
 * 0: Spawn position in Position ATL <ARRAY>
 * 1: Side of spawned group <SIDE>
 * 2: Number of units to spawn <NUMBER>
 * 3: Radius in which to generate waypoints (default: 300) <NUMBER>
 *
 * Return:
 * Spawned group <GROUP>
 *
 * Example:
 * [] call rc_mf_fnc_spawnPatrol
 *
 * Caller: Server
 */

params ["_pos", "_side", "_count", ["_radius", 300]];

private _group = [_pos, _side, _count] call FUNC(spawnGroup);
[_group, _group, _radius, 7] call CBA_fnc_taskPatrol;

{
  if (surfaceIsWater (waypointPosition _x)) then {
    deleteWaypoint _x;
  };
} forEach (waypoints _group);

_group

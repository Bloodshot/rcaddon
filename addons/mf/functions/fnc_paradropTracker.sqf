#include "..\script_component.hpp"
/*
 * Wait for a plane to fly over its paradrop destination, then initiate a
 * paradrop. Will eventually despawn the plane and pilot.
 *
 * Must be spawned
 *
 * Arguments:
 * 0: Paradrop destination in Position 2D <ARRAY>
 * 1: Plane <VEHICLE>
 * 2: Pilot <UNIT>
 * 3: Code to run to initiate paradrop (default: rc_mf_fnc_doParadrop) <CODE>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [markerPos "paradrop", transportPlane, driver transportPlane] call rc_mf_fnc_paradropTracker
 *
 * Caller: Server
 */

params ["_pos", "_plane", "_pilot", ["_callback", FUNC(doParadrop)]];

private _distance = _plane distance2D _pos;
waitUntil {
  sleep 1;

  private _lastDistance = _distance;
  _distance = _plane distance2D _pos;

  // Wait until plane starts getting farther away
  (_distance < 800) && { _distance > _lastDistance }
};

[_plane] call _callback;

sleep 120;
deleteVehicle _pilot;
deleteVehicle _plane;

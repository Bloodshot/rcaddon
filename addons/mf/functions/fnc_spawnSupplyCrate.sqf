#include "..\script_component.hpp"
/*
 * Spawn a supply crate of the given type
 *
 * Arguments:
 * 0: Type of supply crate to spawn, should be in crateMap <STRING>
 * 1: Spawn position in Position ATL
 *
 * Return:
 * Supply crate <OBJECT>
 *
 * Example:
 * ["FIRETEAM", markerPos "supplyMarker"] call rc_mf_fnc_spawnSupplyCrate
 *
 * Caller: Client / Server
 */

params ["_type", "_pos"];

if (isNil "crateMap") exitWith {
  ["crateMap not defined"] call BIS_fnc_error;
  objNull
};

private _idx = _type;
if (typeName _type == "STRING") then {
  _idx = crateMap find _type;
};

if (isNil "_idx" || {_idx == -1}) exitWith {};

private _crate = (GVAR(crates) select _idx);
private _class = (_crate select 1);
private _box = createVehicle [_class, _pos, [], 0, "NONE"];

_box setVariable ["ace_cargo_size", 1, true];
[_box, _crate select 0, _crate select 2] remoteExec [QFUNC(initSupplyCrateLocal), 0];
_box

#include "..\script_component.hpp"
/*
 * Ensure that the player's map is centered on the aoMarker the first time it is
 * displayed.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_centerMapOnAO
 *
 * Caller: Client
 */

private _aoCenter = markerPos "aoMarker";
if (_aoCenter isEqualTo [0, 0]) exitWith {};

waitUntil { [markerPos "aoMarker", 0.1] call FUNC(recenterMap) };

addMissionEventHandler ["Map", {
  params ["_mapIsOpened"];

  if (_mapIsOpened) then {
    waitUntil { [markerPos "aoMarker", 0.1] call FUNC(recenterMap) };
    removeMissionEventHandler ["Map", _thisEventHandler];
  };
}];

#include "..\script_component.hpp"
/*
 * Initializes sides' loadout templates and vehicle pools from objects placed in
 * the editor.
 *
 * Templates are gathered from layers with the following names:
 *   "{SIDE} Template" for the infantry template
 *   "{SIDE} Crew" for the vehicle crew template
 *   "{SIDE} {VEHICLE TYPE}" for each of the vehicle pools
 * where {SIDE} is one of "Opfor", "Blufor", "Indep", or "Civilian", and
 * {VEHICLE TYPE} is one of the vehicle types defined by the vehicleTypes config
 * variable.
 *
 * All objects in each template are deleted. For units, their classname and
 * loadouts are kept. For vehicles, only their classname is.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_initTemplates
 *
 * Caller: Server
 */

private _getLoadouts = {
  private _groupTemplate = getMissionLayerEntities _x;
  if (count _groupTemplate > 0) then {
    private _groups = [];
    {
      if (_x isNotEqualTo grpNull) then { _groups pushBackUnique (group _x) };
    } forEach (_groupTemplate select 0);

    _groups apply {
      private _units = (units _x) apply {
        private _type = typeOf _x;
        private _loadout = getUnitLoadout _x;

        if ([_loadout, getUnitLoadout _type] call BIS_fnc_areEqual) then {
          // Don't save unit loadout if it hasn't been altered
          _loadout = [];
        };

        deleteVehicle _x;

        [_type, _loadout]
      };

      deleteGroup _x;
      _units
    };
  } else {
    []
  };
};

GVAR(groupTemplates) = ["Opfor Template", "Blufor Template", "Indep Template", "Civilian Template"] apply _getLoadouts;
GVAR(crewTemplates) = ["Opfor Crew", "Blufor Crew", "Indep Crew", "Civilian Crew"] apply _getLoadouts;
GVAR(vehicleTemplates) = [nil, nil, nil, nil] apply { createHashMap };

private _getVehicleTypes = {
  params ["_layerName"];

  private _layer = getMissionLayerEntities _layerName;
  if (count _layer == 0) exitWith {[]};

  (_layer select 0) apply {
    private _type = typeOf _x;
    private _customization = [_x] call BIS_fnc_getVehicleCustomization;
    deleteVehicle _x;
    [_type, _customization]
  };
};

{
  private _vehicleGroup = _x;
  private _vehicleLayer = format ["%1s", _vehicleGroup];

  // default vehicles, in case no side-specific template exists
  private _types = [_vehicleLayer] call _getVehicleTypes;
  {
    _x set [_vehicleGroup, _types];
  } forEach GVAR(vehicleTemplates);

  {
    private _layerName = format ["%1 %2", _x, _vehicleLayer];
    private _types = [_layerName] call _getVehicleTypes;
    private _map = GVAR(vehicleTemplates) select _forEachIndex;

    if (count _types > 0) then {
      _map set [_vehicleGroup, _types];
    };
  } forEach ["Opfor", "Blufor", "Indep", "Civilian"];
} forEach (missionNamespace getVariable ["vehicleTypes", [
  "truck",
  "tank",
  "av",
  "apc",
  "ifv"
]]);

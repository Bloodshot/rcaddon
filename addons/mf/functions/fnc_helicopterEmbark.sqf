#include "..\script_component.hpp"
/*
 * Embarks an AI group into a helicopter's passenger seats, preferring turrets
 * and avoiding crew seats. Only assigns the seats, does not move units in
 * instantly.
 *
 * Arguments:
 * 0: Helo to embark the group in <VEHICLE>
 * 1: Group to embark <GROUP>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [vehicle player, aiGroup] remoteExec ["rc_mf_fnc_helicopterEmbark", 2];
 *
 * Caller: Server
 */

params ["_helo", "_group"];

// Assign group units to seats in helicopter, prioritizing door guns/FFV slots

([_helo] call FUNC(emptySeats)) params ["_cargo", "_turrets"];

private _groupUnits = units _group;
for "_i" from 0 to ((count _groupUnits) min (count _turrets)) - 1 do {
  unassignVehicle (_groupUnits select _i);
  (_groupUnits select _i) assignAsTurret [_helo, _turrets select _i];
};

private _turretsSize = count _turrets;
for "_i" from 0 to ((count _groupUnits - _turretsSize) min (count _cargo)) - 1 do {
  unassignVehicle (_groupUnits select (_i + _turretsSize));
  (_groupUnits select (_i + _turretsSize)) assignAsCargoIndex [_helo, _cargo select _i];
};

_groupUnits orderGetIn true;

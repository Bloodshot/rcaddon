#include "..\script_component.hpp"
/*
 * Get the vehicleicon corresponding to the player's role (based on their slot's
 * roleDescription), for use in Diwako DUI and others.
 *
 * Arguments:
 * 0: Unit to get an icon for <UNIT>
 *
 * Return:
 * Path to icon paa <STRING>
 *
 * Example:
 * [player] call rc_mf_fnc_getRoleIcon
 *
 * Caller: Client / Server
 */

params ["_unit"];

_roleName = _unit getVariable ["unitRole", roleDescription _unit];
switch (true) do {
  case (isNil "_unit" || isNil "_roleName"):                       { "\a3\ui_f\data\map\vehicleicons\iconMan_ca.paa" };
  case (leader _unit == _unit):                                    { "\a3\ui_f\data\map\vehicleicons\iconManLeader_ca.paa" };
  case (["lead",                _roleName] call BIS_fnc_inString): { "\a3\ui_f\data\map\vehicleicons\iconManLeader_ca.paa" };
  case (["asst. autorifle",     _roleName] call BIS_fnc_inString): { QPATHTOF(assets\roleicons\aar.paa) };
  case (["asst autorifle",      _roleName] call BIS_fnc_inString): { QPATHTOF(assets\roleicons\aar.paa) };
  case (["asst. machine",       _roleName] call BIS_fnc_inString): { QPATHTOF(assets\roleicons\aar.paa) };
  case (["asst machine",        _roleName] call BIS_fnc_inString): { QPATHTOF(assets\roleicons\aar.paa) };
  case (["assistant autorifle", _roleName] call BIS_fnc_inString): { QPATHTOF(assets\roleicons\aar.paa) };
  case (["ammo bearer",         _roleName] call BIS_fnc_inString): { QPATHTOF(assets\roleicons\ammobearer.paa) };
  case (["ammo carrier",        _roleName] call BIS_fnc_inString): { QPATHTOF(assets\roleicons\ammobearer.paa) };
  case (["grenadier",           _roleName] call BIS_fnc_inString): { QPATHTOF(assets\roleicons\grenadier.paa) };
  case (["marksman",            _roleName] call BIS_fnc_inString): { QPATHTOF(assets\roleicons\marksman.paa) };
  case (["sniper",              _roleName] call BIS_fnc_inString): { QPATHTOF(assets\roleicons\marksman.paa) };
  case (["sharpshooter",        _roleName] call BIS_fnc_inString): { QPATHTOF(assets\roleicons\marksman.paa) };
  case (["autorifle",           _roleName] call BIS_fnc_inString): { "\a3\ui_f\data\map\vehicleicons\iconManMG_ca.paa" };
  case (["machine gun",         _roleName] call BIS_fnc_inString): { "\a3\ui_f\data\map\vehicleicons\iconManMG_ca.paa" };
  case (["medic",               _roleName] call BIS_fnc_inString): { "\a3\ui_f\data\map\vehicleicons\iconManMedic_ca.paa" };
  case (["life saver",          _roleName] call BIS_fnc_inString): { "\a3\ui_f\data\map\vehicleicons\iconManMedic_ca.paa" };
  case (["at",                  _roleName] call BIS_fnc_inString): { "\a3\ui_f\data\map\vehicleicons\iconManAT_ca.paa" };
  case (["rpg",                 _roleName] call BIS_fnc_inString): { "\a3\ui_f\data\map\vehicleicons\iconManAT_ca.paa" };
  case (["demo",                _roleName] call BIS_fnc_inString): { "\a3\ui_f\data\map\vehicleicons\iconManExplosive_ca.paa" };
  default                                                          { "\a3\ui_f\data\map\vehicleicons\iconMan_ca.paa" };
}

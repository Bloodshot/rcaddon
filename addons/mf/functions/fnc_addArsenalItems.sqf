#include "..\script_component.hpp"
/*
 * Populate the player's allowed arsenal items with the contents of
 * rc_mf_allowedArsenalItems and rc_mf_globalArsenalItems. Called automatically
 * from rc_mf_fnc_openArsenal, but needs to be called again if the set of
 * allowed items changes.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_addArsenalItems
 *
 * Caller: Client
 */

private _unitItems = if (isNil { player getVariable ["unitLoadoutIndex", nil] }) then {
  [[], [], [], []]
} else {
  GVAR(allowedArsenalItems) select 1 select (player getVariable "unitLoadoutIndex");
};
private _groupItems = GVAR(allowedArsenalItems) select 0;
private _combined = [0, 1, 2, 3] apply {
  (_unitItems select _x) + (_groupItems select _x) + (GVAR(globalArsenalItems) select _x);
};

private _flattened = flatten _combined;

{
  _flattened pushBackUnique _x;
} forEach (uniqueUnitItems player);

[player, _flattened] call ace_arsenal_fnc_addVirtualItems;

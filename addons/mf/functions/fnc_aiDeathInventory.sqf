#include "..\script_component.hpp"
/*
 * Strips almost all items out of a unit's inventory upon death, leaving a small
 * random selection for players to loot. Does nothing if the unit belongs to one
 * of the friendlySides, or if the unit may have been a player.
 *
 * The *DropChance config variables alter the prevelence of items to drop, but
 * some items (such as NVGs and weapon attachments) will always be removed.
 *
 * Arguments:
 * 0: Unit <UNIT>
 *
 * Return:
 * Nothing
 *
 * Example:
 * [_corpse] call rc_mf_fnc_aiDeathInventory
 *
 * Caller: Server
 */

params ["_unit"];

if (side (group _unit) in (missionNamespace getVariable ["friendlySides", []])) exitWith {};
if (isNull (group _unit)) exitWith {}; // can happen when player disconnects

removeAllAssignedItems _unit;
removeAllPrimaryWeaponItems _unit;
removeAllHandgunItems _unit;

private _aceDropChance = missionNamespace getVariable ["aceDropChance", 1 / 6];
private _medicalDropChance = missionNamespace getVariable ["medicalDropChance", 1 / 3];
private _itemDropChance = missionNamespace getVariable ["itemDropChance", 1 / 6];

{
  private _coef = switch(true) do {
    // Chemlights
    case (["chemlight", toLower _x] call BIS_fnc_inString): { 0 };
    // Radios
    case (["acre_",     toLower _x] call BIS_fnc_inString): { 0 };
    // NVGs
    case (["nvg",       toLower _x] call BIS_fnc_inString): { 0 };
    case (["anpvs",     toLower _x] call BIS_fnc_inString): { 0 };
    // ACE Medical etc.
    case (["ace_",      toLower _x] call BIS_fnc_inString): { _aceDropChance };
    // Vanilla medical
    case (_x in ["FirstAidKit", "Medikit"]):                { _medicalDropChance };
    default                                                 { _itemDropChance };
  };

  if (random 1 > _coef) then { _unit removeItem _x };
} forEach (itemsWithMagazines _unit);

#include "..\script_component.hpp"
/*
 * Spawn a vehicle of a certain type from the given side's vehicle pool, and
 * apply type-specific logic to the vehicle.
 *
 * Arguments:
 * 0: Spawn position in Position ATL <ARRAY>
 * 1: Waypoint position in Position ATL <ARRAY>
 * 2: Vehicle type, from the vehicleTypes configuration variable <STRING>
 * 3: Group's side (default: enemySide, or east) <SIDE>
 *
 * Return:
 * Spawned group <GROUP>
 *
 * Example:
 * [[], markerPos "attackMarker", "tank"] call rc_mf_fnc_spawnVehicleFromPool
 *
 * Caller: Server
 */

params ["_spawnPos", "_targetPos", "_pool", ["_side", missionNamespace getVariable ["enemySide", east]]];

private _truck = (_pool == "truck" || { _pool == "helo" });

private _typeArray = (GVAR(vehicleTemplates) select (_side call BIS_fnc_sideID)) get _pool;
if (isNil "_typeArray") exitWith {
  ["Unknown vehicle pool %1", _pool] call BIS_fnc_error
};

if (count _typeArray == 0) exitWith {};

(selectRandom _typeArray) params ["_type", "_customizations"];
_customizations params ["_variant", "_animations"];

private _group = [_spawnPos, _targetPos, _type, _truck, _side] call FUNC(spawnVehicle);
if (isNil "_group") exitWith {};
private _vic = (vehicle leader _group);

[_vic, _variant, _animations, false] call BIS_fnc_initVehicle;

if (_pool == "av") then {
  [_vic] spawn FUNC(watchAV);
};

_group

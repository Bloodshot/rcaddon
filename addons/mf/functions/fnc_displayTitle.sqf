#include "..\script_component.hpp"
/*
 * Display the mission title after a short delay, using BIS_fnc_textTiles.
 * If the missionTitle config value is missing, use the onLoadName.
 * If both are missing, do not display anything.
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_displayTitle
 *
 * Caller: Client
 */

private _title = missionNamespace getVariable ["missionTitle", getMissionConfigValue "onLoadName"];

if (isNil "_title") exitWith {};

[{
  [parseText (format ["<t font='PuristaBold' size='2.4'>%1</t>", _this]),
   true, nil, 7, 1, 0] call BIS_fnc_textTiles
}, _title, 6] call CBA_fnc_waitAndExecute;

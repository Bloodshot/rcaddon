#include "..\script_component.hpp"
/*
 * Set the player's Diwako DUI vehicleicon based on their role
 *
 * Return:
 * Nothing
 *
 * Example:
 * [] call rc_mf_fnc_setRadarIcon
 *
 * Caller: Client
 */

player setVariable ["diwako_dui_radar_customIcon", [player] call FUNC(getRoleIcon), true];

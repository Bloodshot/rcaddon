#define COMPONENT mf
#define COMPONENT_BEAUTIFIED Mission Framework

#include "\x\rc\addons\main\script_mod.hpp"

#ifdef DEBUG_ENABLED_MF
    #define DEBUG_MODE_FULL
#endif

#include "\x\rc\addons\main\script_macros.hpp"

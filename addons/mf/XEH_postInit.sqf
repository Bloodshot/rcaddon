#include "script_component.hpp"

if !(missionNamespace getVariable [QGVAR(useFramework), false]) exitWith {};

if (isServer) then {
  [QGVAR(deploy), { _this call FUNC(deploy) }] call CBA_fnc_addEventHandler;
  [QGVAR(deployJIP), { _this call FUNC(deployJIP) }] call CBA_fnc_addEventHandler;

  private _vehicleSpawnPositions = getMissionLayerEntities "Vehicle Spawn Positions";
  if (count _vehicleSpawnPositions > 1) then {
    vehicleSpawnPositions = (_vehicleSpawnPositions select 1) apply {
      private _pos = markerPos _x;
      deleteMarker _x;
      _pos
    };
  };

  GVAR(startDate) = date;

  if (missionNamespace getVariable ["startMissionOn", "deploy"] == "auto") then {
    [
      {
        (count allPlayers > 0) && { allPlayers findIf { [_x] call FUNC(isHome) } == -1 }
      },
      FUNC(startMission)
    ] call CBA_fnc_waitUntilAndExecute;
  };

  [{
    if (missionNamespace getVariable [QGVAR(missionStarted), false]) exitWith {
      [_this select 1] call CBA_fnc_removePerFrameHandler;
    };

    private _friendlySides = missionNamespace getVariable ["friendlySides", [west]];
    private _topLeader = nil;
    private _firstPlayer = nil;

    scopeName "outer";
    {
      {
        private _units = (units _x) select { isPlayer _x && alive _x };
        if (count _units > 0) then {
          if (isPlayer (leader _x) && alive (leader _x)) then {
            _topLeader = (leader _x);
            breakTo "outer";
          };

          if (isNil "_firstPlayer") then {
            _firstPlayer = _units select 0;
          };
        };
      } forEach (groups _x);
    } forEach _friendlySides;

    if (isNil "_topLeader") then {
      _topLeader = _firstPlayer;
    };

    if (_topLeader != GVAR(topLeader)) then {
      GVAR(topLeader) = _topLeader;
      publicVariable QGVAR(topLeader);
    };
  }, 5] call CBA_fnc_addPerFrameHandler;
};

if (hasInterface) then {
  addMissionEventHandler ["PlayerViewChanged", {
    params ["", "", "_vehicle"];
    GVAR(playerInAircraft) = (_vehicle isKindOf "Air");
  }];

  [QGVAR(simulationEnabled), {
    if !(_target isKindOf "CAManBase") exitWith { true };

    alive _target && { simulationEnabled _target };
  }] call ace_common_fnc_addCanInteractWithCondition;
};

["veh", {
  params ["_pool", ["_side", missionNamespace getVariable ["enemySide", east]]];

  [
    {
      params ["_pos", "_args", "_shift"];
      _args params ["_pool", "_side"];
      GVAR(mapClick_posArray) pushBack _pos;

      if (count GVAR(mapClick_posArray) == 2) then {
        (GVAR(mapClick_posArray) + [_pool, _side]) remoteExec [QFUNC(spawnVehicleFromPool), 2];
        true
      } else {
        if (_shift) exitWith {
          [[], _pos, _pool, _side] remoteExec [QFUNC(spawnVehicleFromPool), 2];
          true
        };

        false
      };
    },
    [_pool, _side],
    "SCRIPTED",
    [_side, false] call BIS_fnc_sideColor,
    [_side, true] call BIS_fnc_sideColor
  ] call FUNC(mapClick);
}] call FUNC(defineAdminHelper);

{
  [_x, compile (format ['(["%1"] + _this) call veh', _x])] call FUNC(defineAdminHelper);
} forEach (missionNamespace getVariable ["vehicleTypes", [
  "truck",
  "tank",
  "av",
  "apc",
  "ifv"
]]);

["ei", {
  params [["_count", 8], ["_side", missionNamespace getVariable ["enemySide", east]]];

  [{
    params ["_source", "_dest", "_args"];
    _args params ["_side", "_count"];

    [_source, _dest, _side, _count] remoteExec [QFUNC(spawnEnemyInfantry), 2];
  }, [_side, _count], "PAIRS", [_side, false] call BIS_fnc_sideColor, [_side, true] call BIS_fnc_sideColor] call FUNC(mapClick);
}] call FUNC(defineAdminHelper);

["eig", {
  _this params [["_count", 8], ["_side", independent]];
  [_count, _side] call ei;
}] call FUNC(defineAdminHelper);

["patrol", {
  private _side = param [1, enemySide];

  [{
    private _pos = _this select 0;
    (_this select 1) params [["_count", 8], ["_side", missionNamespace getVariable ["enemySide", east]]];
    [_pos, _side, _count] remoteExec [QFUNC(spawnPatrol), 2];
  }, _this, "SINGLE", [_side, false] call BIS_fnc_sideColor, [_side, true] call BIS_fnc_sideColor] call FUNC(mapClick);
}] call FUNC(defineAdminHelper);

["patrols", {
  private _side = param [1, enemySide];

  [{
    private _pos = _this select 0;
    (_this select 1) params [["_count", 8], ["_side", missionNamespace getVariable ["enemySide", east]]];
    [_pos, _side, _count] remoteExec [QFUNC(spawnPatrol), 2];
  }, _this, "MANY", [_side, false] call BIS_fnc_sideColor, [_side, true] call BIS_fnc_sideColor] call FUNC(mapClick);
}] call FUNC(defineAdminHelper);

["bnk", {
  private _side = param [2, missionNamespace getVariable ["enemySide", east]];

  [{
    private _pos = _this select 0;
    _pos set [2, 0];
    (_this select 1) params [["_count", 6], ["_range", 20], ["_side", missionNamespace getVariable ["enemySide", east]]];

    [_pos, _range, _side, _count, 4] remoteExec [QFUNC(garrisonNearby), 2];
  }, _this, "SINGLE", [_side, false] call BIS_fnc_sideColor, [_side, true] call BIS_fnc_sideColor] call FUNC(mapClick);
}] call FUNC(defineAdminHelper);

["garrison", {
  private _side = param [2, missionNamespace getVariable ["enemySide", east]];

  [{
    private _pos = _this select 0;
    _pos set [2, 0];
    (_this select 1) params [["_count", 20], ["_range", 100], ["_side", missionNamespace getVariable ["enemySide", east]]];

    [_pos, _range, _side, _count, 0] remoteExec [QFUNC(garrisonNearby), 2];
  }, _this, "SINGLE", [_side, false] call BIS_fnc_sideColor, [_side, true] call BIS_fnc_sideColor] call FUNC(mapClick);
}] call FUNC(defineAdminHelper);

["crate", {
  [{
    private _pos = _this select 0;
    (_this select 1) params [["_type", "FIRETEAM"]];

    private _ret = [_type, _pos] call FUNC(spawnSupplyCrate);

    if (isNil "_ret") then {
      (format ["Crate type '%1' not found", _type]) call CBA_fnc_notify;
    };

    _ret
  }, _this, "SINGLE", [0.7, 0.6, 0, 1], "ColorUNKNOWN"] call FUNC(mapClick);
}] call FUNC(defineAdminHelper);

["wave", {
  params [
    ["_side", missionNamespace getVariable ["enemySide", east]],
    ["_count", 3],
    ["_armorFactor", missionNamespace getVariable ["armorFactor", 1]],
    ["_spread", 45]
  ];

  [
    {
      ([_this select 0, _this select 1] + (_this select 2)) remoteExec [QFUNC(spawnWave), 2]
    },
    [_side, _count, _armorFactor, _spread],
    "DOUBLE",
    [_side, false] call BIS_fnc_sideColor,
    [_side, true] call BIS_fnc_sideColor
  ] call FUNC(mapClick);
}] call FUNC(defineAdminHelper);

["wavei", {
  params [["_side", missionNamespace getVariable ["enemySide", east]], ["_count", 3], ["_spread", 45]];

  [_side, _count, 0.2, _spread] call wave;
}] call FUNC(defineAdminHelper);

["supplyDrop", {
  [{ [_this select 0] remoteExec [QFUNC(spawnSupplyDrop), 2] }] call FUNC(mapClick);
}] call FUNC(defineAdminHelper);

["paratroopers", {
  params [["_side", missionNamespace getVariable ["enemySide", east]], ["_count", -1], ["_paratrooperVehicle", nil]];

  [{
    (_this select 1) params [["_side", missionNamespace getVariable ["enemySide", east]], ["_count", -1], ["_paratrooperVehicle", nil]];
    [_this select 0, _side, _count, _paratrooperVehicle] remoteExec [QFUNC(spawnParatroopers), 2];
  },
  [_side, _count, _paratrooperVehicle],
  "SINGLE", [_side, false] call BIS_fnc_sideColor, [_side, true] call BIS_fnc_sideColor] call FUNC(mapClick);
}] call FUNC(defineAdminHelper);

["extract", {
  params [["_side", missionNamespace getVariable ["friendlySides", [west]] select 0], ["_helicopterClass", nil]];

  [{
    params ["_pos", "_args", "_shift"];
    _args params [["_side", missionNamespace getVariable ["friendlySides", [west]] select 0], ["_helicopterClass", nil]];
    GVAR(mapClick_posArray) pushBack _pos;

    if (count GVAR(mapClick_posArray) == 2) then {
      [GVAR(mapClick_posArray) select 1, _helicopterClass, GVAR(mapClick_posArray) select 0, nil, _side] remoteExec [QFUNC(spawnExtract), 2];
      true
    } else {
      if (_shift) then {
        [GVAR(mapClick_posArray) select 0, _helicopterClass, nil, nil, _side] remoteExec [QFUNC(spawnExtract), 2];
        true
      } else {
        false
      };
    };
  },
  [_side, _helicopterClass],
  "SCRIPTED", [_side, false] call BIS_fnc_sideColor, [_side, true] call BIS_fnc_sideColor] call FUNC(mapClick);
}] call FUNC(defineAdminHelper);

["bunkermode", FUNC(bunkermode)] call FUNC(defineAdminHelper);

if (missionNamespace getVariable ["startMissionOn", "deploy"] == "spawn") then {
  [QGVAR(adminHelperCalled), {
    params ["_name", ""];

    if (_name in ["ei", "paratroopers", "veh", "wave", "patrol", "patrols"]) then {
      [] remoteExec [QFUNC(startMission), 2];
    };
  }] call CBA_fnc_addEventHandler;
};

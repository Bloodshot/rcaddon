#define PREFIX rc
#define COMPONENT mf
#include "\x\cba\addons\main\script_macros_mission.hpp"

#define SUPER(fncName) (uiNamespace getVariable QFUNC(fncName))
#define ESUPER(addon,fncName) (uiNamespace getVariable QEFUNC(addon,fncName))

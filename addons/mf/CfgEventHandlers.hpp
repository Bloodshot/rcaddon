class Extended_PreInit_EventHandlers {
    class ADDON {
        init = QUOTE(call COMPILE_SCRIPT(XEH_preInit));
    };
};

class Extended_PostInit_EventHandlers {
    class ADDON {
        init = QUOTE(call COMPILE_SCRIPT(XEH_postInit));
    };
};

class Extended_DisplayLoad_EventHandlers {
    class RscCustomInfoMiniMap {
        GVAR(WaypointTracker) = QUOTE(
            params ['_display'];\
            [_display displayCtrl 101] call FUNC(addWaypointTracker);\
            [(_display displayCtrl 13301) controlsGroupCtrl 101] call FUNC(addWaypointTracker););
    };

    class RscDiary {
        GVAR(WaypointTracker) = QUOTE(\
            params ['_display'];\
            [_display displayCtrl 51] call FUNC(addWaypointTracker););
    };

    class RscDisplayEGSpectator {
        GVAR(InitRespawnSpectator) = QUOTE([] call FUNC(initRespawnSpectator););
    };
};
